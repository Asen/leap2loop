package com.d9soft.leap2loop;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;

import com.d9soft.leap2loop.util.Audio;
import com.d9soft.leap2loop.util.L2LData;


public class WebInterface {

    private Activity context = null;

    private WebInterface(){}

    public WebInterface(Activity _context)
    {
        context = _context;
    }

    @JavascriptInterface
    public void Play()
    {
        Intent gameWindow = new Intent(context, GameActivity.class);
        gameWindow.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(gameWindow);
    }

    @JavascriptInterface
    public void D9Soft()
    {
        String link = context.getResources().getString(R.string.d9soft_link);
        context.startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse(link) ) );
    }

    @JavascriptInterface
    public void RateUs()
    {
        //String link = "https://play.google.com/store/apps/details?id=com.antochi.leap2deep.app";
        String link = context.getResources().getString(R.string.market_link);
        context.startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse(link) ) );
    }

    @JavascriptInterface
    public void EnableSound(boolean isOn)
    {
        L2LData.setSoundState(isOn);
        Audio.SetSoundEnabled(isOn);
    }

    @JavascriptInterface
    public void EnableMusic(boolean isOn)
    {
        L2LData.setMusicState(isOn);
        Audio.SetMusicEnabled(isOn);
    }

    @JavascriptInterface
    public boolean GetSoundState()
    {
        return L2LData.getSoundState();
    }

    @JavascriptInterface
    public boolean GetMusicState()
    {
        return L2LData.getMusicState();
    }

}
