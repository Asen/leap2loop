package com.d9soft.leap2loop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.ContextThemeWrapper;
import android.webkit.WebView;

import com.d9soft.leap2loop.util.Audio;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.ServerManagedPolicy;

public class Main extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(BuildConfig.CHECK_LICENSE)
            CheckAppLicense();
        else
            StartGame();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Audio.PauseCoverSound();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Audio.ReStartCover(Audio.Cover.MENU);
        ReloadAds();
    }

    public static void StartActivity()
    {
        Context runningCtx = App.getInstance().getApplicationContext();
        Intent gameWindow = new Intent(runningCtx, Main.class);
        gameWindow.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        runningCtx.startActivity(gameWindow);
    }

    private void ReloadAds()
    {
        try {
            AdView adView = (AdView) Main.this.findViewById(R.id.adView_mainView);
            AdRequest adRequest = new AdRequest.Builder().build();
            if(adView!=null)
                adView.loadAd(adRequest);
        }catch(Exception e)
        {
            App.getInstance().Log("Ads wasn`t loaded in main view: "+e.getMessage());
        }
    }

    private synchronized void StartGame()
    {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                setContentView(R.layout.activity_main);

                // Ads stuff
                ReloadAds();

                // Game`s interface run
                WebView web = (WebView) findViewById(R.id.webView);
                web.getSettings().setBuiltInZoomControls(false);
                web.getSettings().setUseWideViewPort(false);
                web.getSettings().setSupportZoom(false);
                web.getSettings().setJavaScriptEnabled(true);
                web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                web.dispatchSetSelected(false);
                web.setHorizontalScrollBarEnabled(false);
                web.setSelected(false);
                web.addJavascriptInterface(new WebInterface(Main.this), "L2L");
                web.loadUrl("file:///android_asset/start-menu.html");

            }
        });
    }

    private void CheckAppLicense()
    {
        final byte[] SALT = new byte[] {
                0x0F, 0x65, 0x30, 0x18,
                0x30, 0x57, 0x74, 0x64,
                0x51, 0x78, 0x75, 0x45,
                0x77, 0x11, 0x36, 0x11,
                0x11, 0x32, 0x64, 0x79
        };

        final String LICENSE_KEY =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmpLFztPV6/AxM4tAN4a1oOlzF/0ST6I" +
                "VPwWGAr1/875AGBDRIotgX1T1R5UHMUn44z1zVl137dnA5eJ1cTg+OxDYrjh+tERnk3qDMCMeRU2" +
                "6boTHFX9/hCx819QDNdD55Y8CDIsBIYI4voiJEDMckJuSdIPS6/DSIIW6DAWd7uzHhSdYhr5XT3J1g" +
                "dhSuafMn9O4e7KC2v6scHyBHRAejlWN+f7NI/40xULJ3RiIAoghn5SIKjQ/UV+L8h7Z8stlZYl3M5AIh" +
                "WBS7QA+dzZBLrTHQxfLOd0iwiJqf927BwwGYmG1xsP6nlhwJbbvhTpeGSHPV7gK+QNbcDT09JIEoQIDAQAB";

        final String deviceId =
                Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        try {
            AESObfuscator obfuscator = new AESObfuscator(SALT, getPackageName(), deviceId);
            ServerManagedPolicy policy = new ServerManagedPolicy(this, obfuscator);
            LicenseChecker license = new LicenseChecker(getApplicationContext(), policy, LICENSE_KEY);
            /////////////////////////////////////////////////////////////////////////
            license.checkAccess(new L2License(this));
        }catch(Exception e)
        {
            App.getInstance().Log("Error during license check: "+e.getMessage());
            ReportLicenseCheckFailed(L2License.NOT_LICENSED);
        }
    }

    public void ReportLicenseCheckFailed(int code)
    {
        int dialogTheme = R.style.AppStandardDialog;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, dialogTheme));
        alertDialog.setCancelable(false);
        alertDialog.setTitle(R.string.license_failed_caption);
        alertDialog.setMessage(R.string.license_failed_message);
        alertDialog.setNegativeButton(R.string.license_failed_negative_btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Main.this.finish();
            }
        });
        alertDialog.setPositiveButton(R.string.license_failed_positive_btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String marketLink = App.getInstance().getStringRes(R.string.market_link);
                Intent marketPage = new Intent(Intent.ACTION_VIEW, Uri.parse(marketLink));
                Main.this.startActivity(marketPage);

                dialog.dismiss();
                Main.this.finish();
            }
        });
        alertDialog.show();
        App.getInstance().Log("Licence failed. Code: " + code);

        //////////// DEBUG ////////////////
        if(BuildConfig.DEBUG)
            new AlertDialog.Builder(this).
                    setCancelable(true).
                    setTitle("Код ошибки").
                    setMessage(""+code).show();
        ///////////////////////////////////

    }

    public void ReportLicenseCheckSucceed()
    {
        // License check is succeed!
        StartGame();
        App.getInstance().Log("License succeed!");
    }

}
