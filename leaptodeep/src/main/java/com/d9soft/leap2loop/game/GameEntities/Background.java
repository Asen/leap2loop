package com.d9soft.leap2loop.game.GameEntities;


import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.util.RLoader;
import com.d9soft.leap2loop.util.SafeList;
import com.d9soft.leap2loop.util.Texture;

import java.util.Iterator;

public class Background extends GameEntity {

    private static final int BG_COUNT = 20;

    private SafeList<Bitmap> backGround = new SafeList<>();
    private SafeList<Texture> backGroundPieces = new SafeList<>();
    private int pieceWidth = 0;
    private int backgroundNum = 0;


    @Override
    public void Init(){

        super.Init();

        backgroundNum = 0;
        backGround.clear();
        backGroundPieces.clear();

        for(int number=0; number<BG_COUNT; ++number)
        {
            Bitmap bg_texture = RLoader.Graphics.Background.GetParticle(number);
            //int id = App.getInstance().getResIdentifier("bg_" + i, "drawable");
            //Bitmap bg_texture = App.getInstance().decDrawableRes(id);
            float wh_ratio = (float)bg_texture.getWidth() / bg_texture.getHeight();
            int bg_texture_height = L2LConfig.GetSpaceHeight();
            int bg_texture_width = (int) (bg_texture_height * wh_ratio);
            bg_texture = Bitmap.createScaledBitmap(bg_texture, bg_texture_width, bg_texture_height, false);
            backGround.add(bg_texture);
        }

        App.getInstance().Assert(backGround.size()==BG_COUNT, "Background images are missed!");

        pieceWidth = backGround.get(0).getWidth();
        FillBackgroundData();

    }

    @Override
    public void CommonMapMove() {

        if(backGroundPieces.size()==0)
            return;

        backGroundPieces.get(0).X -= Game.GMap.GetTransition();
        for(int i=1; i<backGroundPieces.size(); ++i)
            backGroundPieces.get(i).X = backGroundPieces.get(i-1).X + pieceWidth;

    }

    @Override
    public void PrepareForDrawing()
    {
        if(backGround.size()==0)
            return;

        int lastBackgroundPieceX = backGroundPieces.size()==0 ? 0 :
                (int) backGroundPieces.get(backGroundPieces.size()-1).X;

        // Clearing
        if( lastBackgroundPieceX <= L2LConfig.GetSpaceWidth() )
        {
            FillBackgroundData();
            Iterator it = backGroundPieces.iterator();
            while(it.hasNext()) {
                if (((Texture) it.next()).X <= -pieceWidth)
                    it.remove();
            }
        }

    }

    @Override
    public void Draw(Canvas canvas)
    {
        for(Texture texture : backGroundPieces)
        {
            if(texture.X-pieceWidth > L2LConfig.GetSpaceWidth() || texture.X < -pieceWidth)
                continue;
            canvas.drawBitmap(backGround.get(texture.Num), texture.X, texture.Y, null);
        }
    }

    private void FillBackgroundData()
    {
        if(backGround.size()==0)
            return;

        int lastBackgroundPieceRightX = backGroundPieces.size()==0 ? 0 :
                (int) backGroundPieces.get(backGroundPieces.size()-1).X + pieceWidth;

        int piecesCount = L2LConfig.GetSpaceWidth() / pieceWidth;
        for(int i=0; i<piecesCount; ++i)
        {
            int x = lastBackgroundPieceRightX + i * pieceWidth;
            int pieceId = (backgroundNum++) % BG_COUNT;
            backGroundPieces.add(new Texture(x, 0).setNum(pieceId));
        }
    }

}
