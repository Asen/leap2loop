package com.d9soft.leap2loop.game.GameEntities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.TextView;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.BuildConfig;
import com.d9soft.leap2loop.R;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.util.L2LData;
import com.d9soft.leap2loop.util.RLoader;

import java.util.ArrayList;

public class LearningMode extends GameEntity{

    static class CharacterCrash{
        public boolean crashed = false;

        public void Zero(){
            crashed = false;
        }
    }

    static class CharacterPositions {

        public boolean wasInUpperPos = false, wasInBottomPos = false;
        public boolean inUpperPos = false, inBottomPos = false;
        public boolean shownUpperAnim = false, shownBottomAnim = false;
        public boolean showingUpperAnim = false, showingBottomAnim = false;

        public static boolean isCharacterOnTopFirstTime = true;
        public static boolean wasCharacterOnGroundAfterBottomAnim = false;

        public void Zero(){
            isCharacterOnTopFirstTime = true;
            wasCharacterOnGroundAfterBottomAnim = false;
            wasInBottomPos = wasInUpperPos= false;
            inBottomPos = inUpperPos = false;
            shownBottomAnim = shownUpperAnim = false;
            showingBottomAnim = showingUpperAnim = false;
        }
    }

    static class LearningProcess{
        public final int LEARNING_DISTANCE = 200;
        public final int NORMAL_LEARNING_SCORE = 50;
        public final int MINIMUM_LEARNING_SCORE = 25;
        public boolean wasLearningFinishedDialogShown = false;

        public void Zero(){
            wasLearningFinishedDialogShown = false;
        }
    }

    static class States{
        public static boolean allDialogsShown = false;
        public static boolean isMapShouldMove = false;

        public static String learningMessage = null;
        public static int messageExpiration = 0;

        public static boolean isFlyingExperience = false;
        public static boolean autoPlayComplete = false;
        public static boolean wasAutoPlayModeRun = false;
        public static boolean wasAutoPlayStartedMsgShown = false;
        public static boolean wasAutoPlayFinishedMsgShown = false;

        public static CharacterCrash characterCrash = new CharacterCrash();
        public static LearningProcess learning = new LearningProcess();
        public static CharacterPositions characterPositions = new CharacterPositions();

        public static void Zero()
        {
            allDialogsShown = false;
            isMapShouldMove = false;
            learningMessage = null;
            messageExpiration = 0;
            isFlyingExperience = false;
            autoPlayComplete = false;
            wasAutoPlayModeRun = wasAutoPlayStartedMsgShown = wasAutoPlayFinishedMsgShown = false;

            characterCrash.Zero();
            characterPositions.Zero();
            learning.Zero();
        }
    }


    public enum AutoPlay{MODE_RECORD, MODE_PLAY};
    public enum AutoPlayResult{ NOT_YET, PROCESSING, COMPLETE }


    Game gameInstance = null;
    Dialog activeDialog = null;
    ContextThemeWrapper themedDialog;
    ArrayList<PointF> trajectory = new ArrayList<>();
    int dialogTheme = R.style.LearningModeDialog;


    public LearningMode(Game instance)
    {
        gameInstance = instance;
        themedDialog = new ContextThemeWrapper(gameInstance.getGameContext(), dialogTheme);
    }

    public static boolean isLearningModeState()
    {
        return (L2LData.getLearningModeState() || L2LData.getPlayedGamesAmount()==1) && BuildConfig.LEARNING_ENABLED;
    }

    @Override
    public void Init() {
        super.Init();
        States.Zero();
        ShowIntroDialog();

        gameInstance.getCharacter().DisableUserManagement();
    }

    @Override
    public void CommonMapMove() {
        for(PointF point : trajectory)
            point.x -= Game.GMap.GetTransition();
    }

    @Override
    public void PrepareForDrawing() {
        return;
    }

    @Override
    public void Draw(Canvas canvas) {

        if(!States.allDialogsShown)
            return;

        final DisplayMetrics display = App.getInstance().getDisplayMetrics();
        final int LEARNING_TEXT_SZ = display.heightPixels/30;
        final int CHAR_IN_LINE = 25;

        Paint p = new Paint();
        p.setColor(Color.WHITE);
        p.setTextSize(LEARNING_TEXT_SZ);

        /* Game messages */
        if(GetLearningMessage()!=null) {
            String[] wordsToDraw = GetLearningMessage().split("\\s+");
            String line = "";
            int i = 0, lineNum = 0;
            do {
                line += wordsToDraw[i++] + " ";
                if (line.length() > CHAR_IN_LINE || i == wordsToDraw.length) {
                    float textWidth = p.measureText(line);
                    int x = (int) (display.widthPixels / 2 - textWidth / 2);
                    int y = display.heightPixels / 4 + LEARNING_TEXT_SZ * lineNum;
                    canvas.drawText(line, x, y, p);
                    line = "";
                    lineNum++;
                }
            } while (i < wordsToDraw.length);
        }


        /* Trajectory */
        boolean isUserPushingScreen = gameInstance.getCharacter().IsFingerOnScreen();
        boolean trajectoryUpdate = !isUserPushingScreen;
        if(trajectoryUpdate)
        {
            trajectory.clear();
            trajectory.addAll(gameInstance.getCharacter().CalcTrajectory());
        }
        Paint trajectoryPaint = new Paint();
        int paintWidth = (int) Math.max(2, L2LConfig.GetCharacterHeight()/25f);
        trajectoryPaint.setStrokeWidth(paintWidth);
        trajectoryPaint.setColor(Color.WHITE);
        for(PointF trajectoryPoint : trajectory)
            if(trajectoryPoint.x > gameInstance.getCharacter().getLeftXPos())
                canvas.drawPoint(trajectoryPoint.x, trajectoryPoint.y, trajectoryPaint);


        /* Finger */
        boolean isAutoFly = !gameInstance.getCharacter().IsUserManageable() && gameInstance.getCharacter().IsFingerOnScreen();
        if(isAutoFly)
        {
            Bitmap fingerPush = RLoader.Graphics.Common.GetGameFinger();

            int size = Math.min(fingerPush.getHeight(), display.heightPixels/4);
            fingerPush = Bitmap.createScaledBitmap(fingerPush, size, size, false);
            int x = display.widthPixels/2 - fingerPush.getWidth()/2;
            int y = display.heightPixels/2 - fingerPush.getHeight()/2;
            Paint pausePaint = new Paint();
            pausePaint.setAlpha(150);
            canvas.drawBitmap(fingerPush, x, y, pausePaint);
        }

    }

    private void SetLearningMessage(String msg, int expiration)
    {
        States.learningMessage = msg;
        States.messageExpiration = expiration;
    }

    private String GetLearningMessage()
    {
        if( States.messageExpiration<=0 )
            ifEvent();

        return States.learningMessage;
    }

    private void ifEvent()
    {
        // Trigger when Character in Bottom Hot firstly
        if(States.characterPositions.wasInBottomPos==false && gameInstance.getCharacter().IsInBottomOfLeap())
        {
            gameInstance.Pause();
            States.characterPositions.wasInBottomPos = true;
            States.characterPositions.showingBottomAnim = true;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_in_bottompos), 50);
            return;
        }

        // Triggers when Character in Upper Hot firstly, after Bottom Hot strictly
        if(States.characterPositions.wasInUpperPos==false &&
           States.characterPositions.wasCharacterOnGroundAfterBottomAnim &&
           gameInstance.getCharacter().IsInTopOfLeap())
        {
            gameInstance.Pause();
            States.characterPositions.wasInUpperPos = true;
            States.characterPositions.showingUpperAnim = true;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_in_upperpos), 50);
            return;
        }

        // Triggers after Hot Upper demo
        if(States.isFlyingExperience)
        {
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_fly_learning), 50);
            return;
        }

        // Triggers after User have had his first flying experience
        if(States.autoPlayComplete && !States.wasAutoPlayFinishedMsgShown)
        {
            States.wasAutoPlayFinishedMsgShown =true;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_try_with_obstacles), 5000);
            return;
        }

        // Triggers when AutoPlay just starts
        if(States.wasAutoPlayModeRun && !States.wasAutoPlayStartedMsgShown)
        {
            gameInstance.Pause();
            States.wasAutoPlayStartedMsgShown = true;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_watch_demo), 1500);
            return;
        }

        /* Triggers when character takes his first score point */
        if(States.characterPositions.isCharacterOnTopFirstTime && gameInstance.getCharacter().isCharacterOnTop())
        {
            gameInstance.Pause();
            States.characterPositions.isCharacterOnTopFirstTime = false;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_mode_score_info_msg), 500);
            return;
        }

        /* Triggers when character takes his next score points */
        if(gameInstance.getCharacter().isCharacterOnTop()) {
            States.isMapShouldMove = true;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_points), 1000);
            return;
        }

        /* Triggers when character crashes */
        if(States.characterCrash.crashed && States.autoPlayComplete)
        {
            States.characterCrash.crashed = false;
            SetLearningMessage(App.getInstance().getStringRes(R.string.learning_process_crash), 3000);
            return;
        }

        /* Triggers when character overcomes obstacle */
        if(gameInstance.getSurface().getFirstObstacle()!=null)
        {
            BasicObstacle obstacle = gameInstance.getSurface().getFirstObstacle();
            Boolean isCharOvercameObstacle =
                    gameInstance.getCharacter().getLeftXPos() > obstacle.getLeftOffset()+obstacle.getLength();

            if(isCharOvercameObstacle)
                States.isMapShouldMove = true;

            SetLearningMessage(null, 100);
            return;
        }

        SetLearningMessage(null,100);
    }

    public void ReportTimerTick()
    {
        /* Actual message expiration countdown */
        if(States.messageExpiration>0)
            States.messageExpiration -= L2LConfig.GameOneSecond() / L2LConfig.FPS();

        /* Watching when character have landing after bottom animation */
        if(States.characterPositions.wasInBottomPos==true && !States.characterPositions.showingBottomAnim)
        {
            if(gameInstance.getCharacter().isCharacterOnGround())
                States.characterPositions.wasCharacterOnGroundAfterBottomAnim = true;
        }

        /* If learning mode finished */
        if(!States.learning.wasLearningFinishedDialogShown && States.allDialogsShown) {

            int goneDistance = (int) gameInstance.getCharacter().getGoneDistance().Value();

            boolean isTimeElapsedAndMinimumScoreGotten =
                    (goneDistance > States.learning.LEARNING_DISTANCE) &&
                            gameInstance.getScore()>States.learning.MINIMUM_LEARNING_SCORE;
            boolean isNormalScoreGotten =
                    gameInstance.getScore() >= States.learning.NORMAL_LEARNING_SCORE;

            if (isTimeElapsedAndMinimumScoreGotten || isNormalScoreGotten)
                SignalEndOfLearningDialog();
        }

        /* Stopping of Demo Fly in "bottom hot position" */
        if(States.characterPositions.showingBottomAnim)
        {
            if(gameInstance.getCharacter().getGraphicGeometry().top<=0)
            {
                States.characterPositions.showingBottomAnim = false;
                gameInstance.getCharacter().NoFinger();
            }
        }

        /* Stopping of Demo Fly in "upper hot position" */
        if(States.characterPositions.showingUpperAnim)
        {
            final int GONE_CHARACTERS = 20;
            boolean isFingerOut = !gameInstance.getCharacter().IsFingerOnScreen();
            if(gameInstance.getCharacter().getGoneDistance().Value() >= GONE_CHARACTERS || isFingerOut)
            {
                States.characterPositions.showingUpperAnim = false;
                States.isFlyingExperience = true;
                gameInstance.getCharacter().NoFinger();
                gameInstance.getCharacter().EnableUserManagement();
            }
        }

        // Recording && Autoplay Routine
        if(!States.autoPlayComplete) {

            AutoPlayResult autoStart =
                    gameInstance.getSurface().
                    getSegmentCharacterIn(gameInstance.getCharacter()).
                    GetLearningInterface().
                            ManageAutoPlayState(gameInstance.getCharacter(), AutoPlay.MODE_PLAY);

            if (autoStart == AutoPlayResult.PROCESSING) {
                States.isFlyingExperience = false;
                States.wasAutoPlayModeRun = true;
            }
            if (autoStart == AutoPlayResult.COMPLETE) {
                States.autoPlayComplete = true;
            }
        }
    }

    public void ReportGameResumed()
    {
        /* when user resumes game after "bottom hot position" */
        if(States.characterPositions.wasInBottomPos && !States.characterPositions.shownBottomAnim)
        {
            States.characterPositions.shownBottomAnim = true;
            gameInstance.getCharacter().Finger();
        }

        /* when user resumes game after "upper hot position" */
        if(States.characterPositions.wasInUpperPos && !States.characterPositions.shownUpperAnim)
        {
            States.characterPositions.shownUpperAnim = true;
            gameInstance.getCharacter().Finger();
        }

    }

    public boolean isMapPassive()
    {
        return !States.isMapShouldMove;
    }

    public void ReportCharacterCrash()
    {
        States.characterCrash.crashed = true;
    }

    private void __KillActiveDialog()
    {
        if(activeDialog!=null)
            activeDialog.cancel();
        activeDialog = null;
    }

    private void ShowIntroDialog()
    {
        activeDialog = new Dialog(themedDialog);
        activeDialog.setContentView(R.layout.learning_mode_dialog);
        activeDialog.setTitle(R.string.fanfic_caption);
        activeDialog.setCancelable(false);
        ((TextView)activeDialog.findViewById(R.id.learning_text_view)).setText(R.string.fanfic);
        activeDialog.findViewById(R.id.learning_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                __KillActiveDialog();
                ShowMotionDialog();
            }
        });

        activeDialog.show();
    }

    private void ShowMotionDialog()
    {
        final int[] MSG_LEVEL = {0};
        final String[] Messages = App.getInstance().getArrayRes(R.array.learning_process);

        activeDialog = new Dialog(themedDialog);
        activeDialog.setContentView(R.layout.learning_mode_dialog);
        activeDialog.setTitle(R.string.learning_process_title);
        activeDialog.setCancelable(false);
        activeDialog.show();

        final TextView info = (TextView)activeDialog.findViewById(R.id.learning_text_view);
        info.setText(Messages[MSG_LEVEL[0]]);

        activeDialog.findViewById(R.id.learning_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (++MSG_LEVEL[0] >= Messages.length) {
                    __KillActiveDialog();
                    States.allDialogsShown = true;
                } else
                    info.setText(Messages[MSG_LEVEL[0]]);
            }
        });

    }

    private void SignalEndOfLearningDialog()
    {
        gameInstance.Pause();

        final AlertDialog.Builder alert = new AlertDialog.Builder(themedDialog);
        alert.setCancelable(false);
        alert.setTitle(R.string.learning_mode_finished);
        alert.setMessage(R.string.learning_mode_finished_msg);
        alert.setPositiveButton(R.string.learning_mode_finished_positive_btn, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                __KillActiveDialog();
                L2LData.setLearningModeState(false);
                gameInstance.Restart();
            }
        });

        activeDialog = alert.show();
        States.learning.wasLearningFinishedDialogShown = true;
    }

}
