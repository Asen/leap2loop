package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.GameEntities.Character;
import com.d9soft.leap2loop.game.GameEntities.LearningMode;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator.Segment_WithoutGround;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator.VariantPicker;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.RLoader;
import com.d9soft.leap2loop.util.SafeList;

public class Segment {

    private SafeList<BasicObstacle> objects = new SafeList<>();
    private SegmentConfiguration Config = new SegmentConfiguration();
    private LearningInterface learningInterface = null;
    private long id = System.currentTimeMillis();
    private Ground segmentGround = null;
    private float leftBorder = 0;
    private float rightBorder = 0;
    private int width = 0;

    @Override
    public boolean equals(Object o) {
        return this.id == ((Segment)o).id;
    }

    public Segment(final float segStartX, final Game.ComplexityLevel complexity)
    {
        Class segmentType = VariantPicker.PickSegmentClass(complexity);
        App.getInstance().Log("==========Start Segment Creation : " + (segmentType));

        VariantPicker.PickSegment(segmentType, objects);
        ConfigureSegment(segmentType);

        float obstacleXPos = (segStartX) + L2LConfig.GetDefaultObstacleBetweenDistance().Calc();
        for(BasicObstacle object:objects) {
            object.setLeftOffset(obstacleXPos);
            obstacleXPos += object.getLength() + object.getForwardDistance();
        }

        BasicObstacle lastObject = objects.getLast();
        leftBorder = segStartX;
        rightBorder = lastObject==null ? leftBorder : (int) (lastObject.getLeftOffset() + lastObject.getLength());
        width = (int) (rightBorder-leftBorder);

        App.getInstance().Assert(width > 0, "Right segment border("+rightBorder+") is lower than left("+leftBorder+"):");
        App.getInstance().Log("==========Finish Segment creation(startX=" + leftBorder + "; endX=" + rightBorder + ")");
        segmentGround = new Ground();

    }

    public Segment(final int startX, final int endX)
    {
        leftBorder = startX;
        rightBorder = endX;
        width = (int) (rightBorder - leftBorder);
        App.getInstance().Assert(width > 0, "Right segment border(" + rightBorder + ") is lower than left(" + leftBorder + "):");
        segmentGround = new Ground();
    }

    private void ConfigureSegment(Class segmentClass)
    {
        if(segmentClass == Segment_WithoutGround.class)
            Config.disableGroundJumps();
    }

    public Segment.LearningInterface GetLearningInterface()
    {
        if(learningInterface ==null)
            learningInterface = new LearningInterface();
        return learningInterface;
    }

    public float getLeftBorder()
    {
        return leftBorder;
    }

    public float getRightBorder()
    {
        return rightBorder;
    }

    public BasicObstacle getFirstObstacle()
    {
        return objects.getFirst();
    }

    public BasicObstacle getObstacleCharacterOn(final Character character)
    {
        for(BasicObstacle obstacle : objects)
            if( obstacle.isCharacterOnSurface(character) )
                return obstacle;
        return null;
    }

    public boolean isCharacterCrashed(final Character character)
    {
        for(BasicObstacle obstacle:objects)
            if(obstacle.isCharacterIntersected(character)) return true;
        return false;
    }

    public void MoveInnerObjects()
    {
        for(int i=0; i<objects.size(); ++i)
            objects.get(i).parallelMove();
    }

    public void SetX(float newX)
    {
        leftBorder = newX;
        rightBorder = leftBorder + width;
    }

    public void ReactOnLanding(BasicObstacle obstacleCharacterOn)
    {
        /* Reaction on current obstacle in case of: */
        if(obstacleCharacterOn instanceof PlatformLoweredObstacle)
            obstacleCharacterOn.ReactOnLanding((PlatformObstacle) obstacleCharacterOn);

        /* And reaction on all further obstacles in case of: */
        for(BasicObstacle obstacle:objects)
            if(obstacle instanceof DoubleObstacle)
                obstacle.ReactOnLanding((PlatformObstacle) obstacleCharacterOn);
    }

    public void onCharacterMoved(Character character)
    {
        Config.Event(character);
    }

    public void Draw(Canvas canvas)
    {
        for(BasicObstacle obstacle : objects){
            int rightObstacleBorder = (int) (obstacle.getLeftOffset()+obstacle.getLength());
            if(rightObstacleBorder>=0)
                canvas.drawBitmap(
                        obstacle.getDrawable(),
                        obstacle.getLeftOffset(),
                        obstacle.getTopOffset(),
                        null);

            /*for(RectF r : obstacle.getGeometryBuffer())
            {
                Paint p = new Paint();
                p.setColor(Color.RED);
                p.setStrokeWidth(2f);
                canvas.drawRect(r, p);
            }*/
        }

        segmentGround.Draw(canvas);
    }


    private class Ground{

        private Bitmap ground_texture = null;
        private Rect initialGroundRect = null;
        private int groundWidth = 0, groundHeight = 0;

        public Ground()
        {
            ground_texture = (Config.isGroundJumpsAllowed) ?
                    RLoader.Graphics.Ground.GetGround() :
                    RLoader.Graphics.Ground.GetLava();

            groundHeight = App.getInstance().getDisplayMetrics().heightPixels - L2LConfig.GetGroundLevel();
            // 0.07 is a particle of bottom transparent space.
            // this helps to hide possible artifacts(for lowered obstacles):
            groundHeight *= 1.07f;

            float wh_ratio = ground_texture.getWidth() / ground_texture.getHeight();
            groundWidth = (int) (groundHeight * wh_ratio);

            initialGroundRect = new Rect(0, 0, ground_texture.getWidth(), ground_texture.getHeight());

            /*ground = Bitmap.createBitmap(width, groundHeight, Bitmap.Config.ARGB_8888);
            Canvas ground_canvas = new Canvas(ground);
            Rect initialGroundRect = new Rect(0, 0, ground_texture.getWidth(), ground_texture.getHeight());
            for (int i = 0; i <= Math.floor(width / groundWidth); ++i)
                ground_canvas.drawBitmap(
                        ground_texture,
                        initialGroundRect,
                        new Rect(i * groundWidth, 0, (i + 1) * groundWidth, groundHeight),
                        null
                );*/
        }

        public void Draw(Canvas canvas)
        {
            /*float x = Segment.this.getLeftBorder();
            float y = L2LConfig.GetGroundLevel();
            canvas.drawBitmap(ground, x, y, null);*/

            for (int i = 0; i <= Math.floor(width / groundWidth); ++i) {

                int leftPos = (int) (getLeftBorder() + i * groundWidth);
                int rightPos = (int) (getLeftBorder() + (i+1) * groundWidth);
                int top = L2LConfig.GetGroundLevel();
                int bottom = L2LConfig.GetGroundLevel() + groundHeight;

                if(rightPos>0)
                    canvas.drawBitmap(
                            ground_texture,
                            initialGroundRect,
                            new Rect(leftPos, top, rightPos, bottom),
                            null
                    );
            }

        }

    }


    private class SegmentConfiguration{

        private boolean isGroundJumpsAllowed = true;

        private void disableGroundJumps()
        {
            isGroundJumpsAllowed = false;
        }

        private void Event(Character character)
        {
            if(!isGroundJumpsAllowed && character.isCharacterOnGround())
                character.Kill();
        }

    }

    /////////////////////////////////////////////////////////////////
    //  LearningMode Functionality
    ////////////////////////////////////////////////////////////////

    public class LearningInterface
    {
        private BasicObstacle GetObstacleAutoplayStartsFrom(final Character character)
        {
            RectF charRect = character.getGraphicGeometry();
            BasicObstacle obstacle = null;
            for(int i=0; i<objects.size(); ++i)
                if(objects.get(i) instanceof PlatformObstacle && objects.get(i).leftOffset>charRect.right)
                {
                    obstacle = objects.get(i);
                    break;
                }
            return obstacle;
        }


        public LearningMode.AutoPlayResult ManageAutoPlayState(final Character character, LearningMode.AutoPlay mode)
        {
            RectF charRect = character.getGraphicGeometry();

            // Stop
            float finishDistance = getRightBorder() - charRect.right;
            if(mode == LearningMode.AutoPlay.MODE_PLAY)
            {
                boolean isCompletedAlready = character.GetLearningInterface().IsLearningComplete();
                boolean isStopPointReached = finishDistance <= new CCountW(2f).Calc();
                if(isCompletedAlready || isStopPointReached) {
                    character.GetLearningInterface().DisableLearning();
                    return LearningMode.AutoPlayResult.COMPLETE;
                }
            }else{
                boolean isStopPointReached = finishDistance <= new CCountW(2f).Calc();
                if(isStopPointReached) {
                    character.GetLearningInterface().StopRecordingMode();
                    return LearningMode.AutoPlayResult.COMPLETE;
                }
            }


            if(character.GetLearningInterface().IsRunning())
                return LearningMode.AutoPlayResult.PROCESSING;


            BasicObstacle platform = GetObstacleAutoplayStartsFrom(character);
            if(platform==null)
                return LearningMode.AutoPlayResult.NOT_YET;


            // Start
            float startDistance = platform.leftOffset - charRect.right;
            if(startDistance <= new CCountW(5f).Calc())
            {
                if (mode == LearningMode.AutoPlay.MODE_RECORD)
                    character.GetLearningInterface().PrepareForRecording(charRect.right);
                else
                    character.GetLearningInterface().PrepareForLearning(charRect.right);

                return LearningMode.AutoPlayResult.PROCESSING;
            }

            return LearningMode.AutoPlayResult.NOT_YET;
        }
    }

}
