package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;

import com.d9soft.leap2loop.game.L2LConfig;

public class PlatformLoweredObstacle extends PlatformObstacle {

    public PlatformLoweredObstacle(PlatformObstacle.Customization customization)
    {
        super(customization);
    }

    @Override
    public void ReactOnLanding(PlatformObstacle obstacle) {
        this.topOffset += Math.abs(this.Value);

        // If obstacle is lower than ground than put as down as possible:
        if(this.topOffset > L2LConfig.GetGroundLevel())
            this.topOffset = L2LConfig.GetSpaceHeight()*2;

        _GeometryChanged();

    }
}
