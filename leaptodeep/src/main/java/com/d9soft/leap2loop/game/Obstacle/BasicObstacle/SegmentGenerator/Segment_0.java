package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;

import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BottomObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.DoubleObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformZeroObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.UpperObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountH;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;

import java.util.Collection;

public class Segment_0 {

    public static void V0(Collection<BasicObstacle> box)
    {
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.3f),new ScrPartH(0.4f)).
                                setWidth(new CCountW(2f), new CCountW(3f)).
                                setForwardDistance(new CCountW(5f))
                )
        );
        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.2f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(2f), new CCountW(3f)).
                                setForwardDistance(new CCountW(4f))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.3f), new ScrPartH(0.4f)).
                                setWidth(new CCountW(2f), new CCountW(3f))
                )
        );
    }

    public static void V1(Collection<BasicObstacle> box)
    {
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.15f),new ScrPartH(0.2f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3.5f)).
                                setForwardDistance(new CCountW(5f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f))
                )
        );
    }

    public static void V2(Collection<BasicObstacle> box)
    {
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.35f)).
                                setWidth(new CCountW(2f), new CCountW(2.5f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.2f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setForwardDistance(new CCountW(1f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.35f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3f)).
                                setForwardDistance(new CCountW(4f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(1.5f), new CCountW(2.5f))
                )
        );
    }

    public static void V3(Collection<BasicObstacle> box)
    {
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3f)).
                                setForwardDistance(new CCountW(0f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.35f), new ScrPartH(0.4f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.3f), new ScrPartH(0.35f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
    }

    public static void V4(Collection<BasicObstacle> box)
    {

        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.2f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.35f)).
                                setWidth(new CCountW(2f), new CCountW(2.5f)).
                                setValue(new ScrPartH(-0.025f), new ScrPartH(-0.010f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.1f),new ScrPartH(0.15f)).
                                setWidth(new CCountW(2f), new CCountW(2.25f)).
                                setFreeSpaceHeight(new CCountH(3.5f))
                )
        );
    }

}
