package com.d9soft.leap2loop.game;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.BuildConfig;
import com.d9soft.leap2loop.GameActivity;
import com.d9soft.leap2loop.R;
import com.d9soft.leap2loop.game.GameEntities.Background;
import com.d9soft.leap2loop.game.GameEntities.Character;
import com.d9soft.leap2loop.game.GameEntities.LearningMode;
import com.d9soft.leap2loop.game.GameEntities.Surface;
import com.d9soft.leap2loop.game.GameEntities.Texts;
import com.d9soft.leap2loop.util.Audio;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.L2LData;
import com.d9soft.leap2loop.util.PPS.PPS;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Game extends View {

    private enum GameState{ UNINITIALIZED, PAUSED, RUN }
    ////////////////////////////////////////////////////////////
    public enum ComplexityLevel {
        Level_Learning_MapPassive,
        Level_Learning_MapActive,
        LVL_0,
        LVL_50,
        LVL_100,
        LVL_150,
        LVL_200,
        LVL_250,
        LVL_300
    }
    private static ComplexityLevel ComplexityLvl = ComplexityLevel.LVL_0;
    private LevelSetter LvlSetter = new LevelSetter();
    /////////////////////////////////////////
    private boolean __isLearningMode = false;
    /////////////////////////////////////////
    private GameActivity gameContext = null;
    private Dialog loseDialog = null;
    //////////////////////////////////////////
    private ScheduledExecutorService executer = null;
    private Integer executerInterval = 0;
    private Runnable execTask = null;
    private GameState gameState = GameState.UNINITIALIZED;
    //////////////////////////////////////////
    private int score = 0;
    private int actualOrientation = Configuration.ORIENTATION_UNDEFINED;
    ////////////// Graphic Data //////////////
    private final Background background = new Background(); // too much resources absorbing
    //private final Ground ground = new Ground();             // too much resources absorbing
    private Surface surface = null;
    private Character character = null;
    private Texts textes = null;
    private LearningMode learningMode = null;


    public Game(Context context) {
        super(context);
    }
    public Game(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public Game(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(w, h);
    }


    public Context getGameContext(){ return gameContext; }
    public Surface getSurface(){ return surface; }
    public int getScore(){ return score; }
    public Character getCharacter(){ return character; }
    public static ComplexityLevel getLevel(){ return ComplexityLvl; }


    public boolean isGameLost()
    {
        boolean isLoseDialogShowing = loseDialog!=null && loseDialog.isShowing();
        return isLoseDialogShowing || surface.isCharacterCrashed(character);
    }
    public boolean isGamePaused()
    {
        return gameState == GameState.PAUSED;
    }
    public void AddScore(int add){
        score+=add;
    }

    public void ReportCharacterScore(){
        Audio.PlayScore();
    }
    public void ReportCharacterLeap(){
        Audio.PlayLeap();
    }
    private void ReportCharacterCrash(){
        Audio.PlayCrash();
        App.getInstance().Vibrate(150);
        App.getInstance().Log("[LOST_GAME]");
    }

    /* this method calls from calling context when config changes/first start */
    /* Game lifecycle starts here */
    public void Run(GameActivity context)
    {
        L2LConfig.Set();
        L2LData.updatePlayedGamesAmount();
        GMap.InitStatics();
        App.getInstance().Log("Game is Run");

        _PrepareData(context);
        _PrepareAnimator();
        Resume();

        gameContext.UpdateAfterGameStart();
        Audio.PlayIceCrack();
        Audio.ReStartCover(Audio.Cover.GAME);
    }

    public void Pause()
    {
        // already paused
        if(gameState == GameState.PAUSED)
            return;

        gameState = GameState.PAUSED;
        executer.shutdownNow();
        Audio.PauseAll();
        App.getInstance().Log("Game is Paused");

        gameContext.UpdateAfterGameStart();
        invalidate();
    }

    public void Resume()
    {
        // already running
        if(gameState == GameState.RUN)
            return;

        gameState = GameState.RUN;
        executer = Executors.newSingleThreadScheduledExecutor();
        executer.scheduleAtFixedRate(execTask, 0, executerInterval, TimeUnit.MILLISECONDS);
        Audio.ResumeAll();

        if(__isLearningMode)
            learningMode.ReportGameResumed();

        App.getInstance().Log("Game is Resumed");
        gameContext.UpdateAfterGameStart();
    }

    public void Restart()
    {
        this.Pause();

        App.getInstance().Log("Game is Restarted");
        this.Run(gameContext);
    }

    public void ReportExit()
    {
        this.Pause();
        App.getInstance().Log("Game exiting...");
        if(__isLearningMode){
            L2LData.setLearningModeState(false);
        }

        character = null;
        surface = null;
        learningMode = null;

    }

    public void Lose()
    {
        if(gameState == GameState.PAUSED) return;

        if(!__isLearningMode) {
            Pause();
            ReportCharacterCrash();
            _ProcessResults();
            _ShowLoseDialog();
        }else
        {
            Audio.PlayCrash();
            App.getInstance().Vibrate(150);
            //character.MoveToStartPosition();
            learningMode.ReportCharacterCrash();
        }
    }

    /* this method calls when config changes/first start/restart */
    private void _PrepareData(GameActivity context) {

        /* learning mode */
        __isLearningMode = false;
        /* score zeroing */
        score = 0;

        /* main structures initialization */
        gameContext = context;

        this.character = new Character(this);
        this.textes = new Texts(this);
        this.surface = new Surface();

        // Learning mode
        if(LearningMode.isLearningModeState())
            _EnableLearningMode();
        else
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_0);

        /* A lot of memory absorbing otherwise */
        Configuration config = getResources().getConfiguration();
        if(config.orientation != actualOrientation)
        {
            actualOrientation = config.orientation;
            background.Init();
        }
        /////////////////////////////////////////////

        // Initialization of lose-dialog
        _PrepareLoseDialog();

    }

    private void _PrepareAnimator()
    {
        executerInterval = L2LConfig.GameOneSecond() / L2LConfig.FPS();

        execTask = new Runnable() {
            @Override
            public void run() {

                gameContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        /* Calculation and Redraw */
                        _DoDrawPreparations();
                        invalidate();

                        /* If game lost */
                        if(isGameLost()) {
                            if(!BuildConfig.GOD_MODE) Lose();
                            App.getInstance().Log("[GAME_LOST] Game`s lost");
                        }

                        /* Resolving learning logic */
                        if (__isLearningMode)
                            learningMode.ReportTimerTick();

                        _IncreaseGameLevel();
                    }
                });
            }
        };

    }

    private void _DoDrawPreparations() {

        // Preparation. Here can be code which touches mapspeed
        background.PrepareForDrawing();
        surface.PrepareForDrawing();
        character.PrepareForDrawing();
        textes.PrepareForDrawing();
        if(__isLearningMode)
            learningMode.PrepareForDrawing();

        // Map speed can be changed depending set of factors:
        _PickMapSpeed();

        // Moving objects with the same speed as map moves
        background.CommonMapMove();
        surface.CommonMapMove();
        character.CommonMapMove();
        textes.CommonMapMove();
        if(__isLearningMode)
            learningMode.CommonMapMove();
    }

    private void _PickMapSpeed()
    {
        // Factor1: Map character "Pushing" the map
        if(character.IsOnTheMapEdge())
        {
            PPS speedPerSec = new PPS(character.GetCharacterSpeedPerFrame()*L2LConfig.FPS());
            GMap.InjectSpeed(new GMap.CustomMapSpeedInfo(speedPerSec), GMap.InjectionId.PUSH);
        }else{
            GMap.WithdrawInjectedSpeed(GMap.InjectionId.PUSH);
        }

        
        // Factor2: Map speed increase level
        if(!__isLearningMode) {
            float DIST_START = 400f;
            float DIST_FINISH = 500f;
            float MIN_SPEED = 1f;
            float MAX_SPEED = 2f;

            float goneChars = character.getGoneDistance().Value();

            if (DIST_START < goneChars && goneChars <= DIST_FINISH) {
                float speedInChars = (MIN_SPEED) + (MAX_SPEED - MIN_SPEED) * ((goneChars - DIST_START) / (DIST_FINISH - DIST_START));
                float speedInPx = new CCountW(speedInChars).Calc();

                GMap.SetSpeed(new GMap.CustomMapSpeedInfo(new PPS(speedInPx)));
                //App.getInstance().Log("[Aspeed] per chars: " + newSpeed);
            }
        }

    }


    private void _IncreaseGameLevel()
    {
        if(__isLearningMode) {

            if(learningMode.isMapPassive())
                LvlSetter.SetComplexityLevel(ComplexityLevel.Level_Learning_MapPassive);
            else
                LvlSetter.SetComplexityLevel(ComplexityLevel.Level_Learning_MapActive);

            return;
        }

        if(score==50)
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_50);
        if(score==100)
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_100);
        if(score==150)
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_150);
        if(score==200)
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_200);
        if(score==250)
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_250);
        if(score==300)
            LvlSetter.SetComplexityLevel(ComplexityLevel.LVL_300);
    }

    private void _EnableLearningMode()
    {
        if(__isLearningMode) return;

        //gameContext.setTitle(R.string.learning_mode_title);

        /* init learningMode module */
        learningMode = new LearningMode(this);
        learningMode.Init();
        __isLearningMode = true;

        // stop game motion
        LvlSetter.SetComplexityLevel(ComplexityLevel.Level_Learning_MapPassive);
    }

    private void _PrepareLoseDialog()
    {
        if(loseDialog != null) return;

        loseDialog = new Dialog(gameContext, R.style.GameoverDialog);
        loseDialog.setContentView(R.layout.game_over_window);
        loseDialog.setTitle(App.getInstance().getStringRes(R.string.app_name));
        loseDialog.setCancelable(false);

        final WebView webView = (WebView)loseDialog.findViewById(R.id.webView2);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.dispatchSetSelected(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setSelected(false);
        webView.addJavascriptInterface(new GameoverInterface(), "L2L");
        webView.loadUrl("file:///android_asset/gameover.html");
    }

    private void _ShowLoseDialog()
    {
        if(loseDialog==null) _PrepareLoseDialog();
        if(loseDialog.isShowing()) return;

        loseDialog.show();

        final AdView adView = (AdView)loseDialog.findViewById(R.id.adView_loseDialog);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    private void _ProcessResults()
    {
        _SaveResults();
        _UpdateScoreTable();
        _UnlockAchievements();
    }

    private void _SaveResults()
    {
        int distance = (int) character.getGoneDistance().Value();
        L2LData.trySaveNewScore(score);
        L2LData.trySaveNewDistance(distance);
    }

    private void _UpdateScoreTable()
    {
        int distance = (int) character.getGoneDistance().Value();
        gameContext.getGPGSInterface().PostBestScore_Points();
        gameContext.getGPGSInterface().PostBestScore_Distance();
        gameContext.getGPGSInterface().UpdateScoreTable(score, distance);
    }

    private void _UnlockAchievements()
    {
        gameContext.getGPGSInterface().UnlockAchievements();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(character.IsUserManageable())
        {
            switch(event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    if(!character.IsFingerOnScreen())
                        character.Finger();
                    break;
                case MotionEvent.ACTION_UP:
                    if(character.IsFingerOnScreen())
                        character.NoFinger();
                    break;
            }
        }

        if(event.getAction()==MotionEvent.ACTION_DOWN)
        {
            if(isGamePaused() && !isGameLost())
                Resume();
        }

        return true;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        if(gameContext==null)
            return;

        gameContext.UpdateGameInfo((int) character.getGoneDistance().Value(), getScore());
        canvas.drawColor(Color.BLACK);

        background.Draw(canvas);
        surface.Draw(canvas);
        character.Draw(canvas);
        textes.Draw(canvas);

        if(__isLearningMode)
            learningMode.Draw(canvas);

    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    private class GameoverInterface
    {
        @JavascriptInterface
        public void RestartGame()
        {
            loseDialog.cancel();
            Audio.PlayClick();
            Run(gameContext);
        }

        @JavascriptInterface
        public void QuitGame()
        {
            loseDialog.cancel();
            Audio.PlayClick();
            Pause();
            gameContext.finish();
        }

        @JavascriptInterface
        public void RunLearning()
        {
            loseDialog.cancel();
            L2LData.setLearningModeState(true);
            Audio.PlayClick();
            Run(gameContext);
        }

        @JavascriptInterface
        public void LookRecords() {
            gameContext.getGPGSInterface().getLeaderboardGPGS();
            Audio.PlayClick();
        }

        @JavascriptInterface
        public String GetUserResult()
        {
            int distance = (int) getCharacter().getGoneDistance().Value();
            return score+";"+distance+"m";
        }

        @JavascriptInterface
        public String GetBestUserResult()
        {
            int distance = Math.max(
                    gameContext.getGPGSInterface().GetBestUserDistance(),
                    (int) getCharacter().getGoneDistance().Value());
            int points = Math.max(
                    gameContext.getGPGSInterface().GetBestUserPoints(),
                    score);

            return points+";"+distance+"m";
        }

        @JavascriptInterface
        public String GetWeeklyResult()
        {
            int distance = Math.max(
                    gameContext.getGPGSInterface().GetWeeklyDistance(),
                    (int) getCharacter().getGoneDistance().Value());
            int points = Math.max(
                    gameContext.getGPGSInterface().GetWeeklyPoints(),
                    score);

            return points+";"+distance+"m";
        }

        @JavascriptInterface
        public String GetBestResult()
        {
            int distance = Math.max(
                    gameContext.getGPGSInterface().GetBestDistance(),
                    (int) getCharacter().getGoneDistance().Value());
            int points = Math.max(
                    gameContext.getGPGSInterface().GetBestPoints(),
                    score);

            return points+";"+distance+"m";
        }

        @JavascriptInterface
        public String GetTitle()
        {
            int bestUserDistance=gameContext.getGPGSInterface().GetBestUserDistance();
            int bestUserPoints=gameContext.getGPGSInterface().GetBestUserPoints();
            int goneDistance = (int)character.getGoneDistance().Value();
            int scorePoints = score;

            boolean isNewPointsRecord = scorePoints>=bestUserPoints && scorePoints>=10;
            boolean isNewDistanceRecord = goneDistance>=bestUserDistance && goneDistance>=10;

            if(isNewPointsRecord)
                return getResources().getString(R.string.lose_title_points_record);
            if(isNewDistanceRecord)
                return getResources().getString(R.string.lose_title_distance_record);
            if(isNewDistanceRecord && isNewPointsRecord)
                return getResources().getString(R.string.lose_title_full_record);

            return getResources().getString(R.string.lose_title_default);
        }

        @JavascriptInterface
        public String Localize(String text)
        {
            if(text.compareToIgnoreCase("title_result")==0)
                return getResources().getString(R.string.lose_result);
            if(text.compareToIgnoreCase("title_record")==0)
                return getResources().getString(R.string.lose_record);
            if(text.compareToIgnoreCase("look_records")==0)
                return getResources().getString(R.string.lose_record_view);
            if(text.compareToIgnoreCase("restart")==0)
                return getResources().getString(R.string.lose_again);
            if(text.compareToIgnoreCase("quit")==0)
                return getResources().getString(R.string.lose_exit);
            if(text.compareToIgnoreCase("title_record")==0)
                return getResources().getString(R.string.lose_record);
            if(text.compareToIgnoreCase("title_week")==0)
                return getResources().getString(R.string.lose_records_weekly);
            if(text.compareToIgnoreCase("title_alltime")==0)
                return getResources().getString(R.string.lose_records_alltime);
            if(text.compareToIgnoreCase("learning")==0)
                return getResources().getString(R.string.lose_goto_learning);

            return "";
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////

    private class LevelSetter
    {
        private void SetComplexityLevel(ComplexityLevel level_id)
        {
            Game.this.ComplexityLvl = level_id;

            switch(level_id)
            {
                case LVL_0:
                    GMap.SetSpeed( new GMap.MapSpeedInfo(GMap.MapSpeed.SPEED_1) );
                    //App.getInstance().Log("Level: 0");
                    break;
                case LVL_50:
                    //**//
                    break;
                case LVL_100:
                    //**//
                    break;
                case LVL_150:
                    //**//
                    break;
                case LVL_200:
                    //**//
                    break;
                case LVL_250:
                    //**//
                    break;
                case LVL_300:
                    //**//
                    break;
                case Level_Learning_MapPassive:
                    GMap.SetSpeed( new GMap.MapSpeedInfo(GMap.MapSpeed.SPEED_0) );
                    //App.getInstance().Log("Level: Map_Stopped");
                    break;
                case Level_Learning_MapActive:
                    GMap.SetSpeed(new GMap.MapSpeedInfo(GMap.MapSpeed.SPEED_1));
                    //App.getInstance().Log("Level: Map_Active");
                    break;
                default:
                    //App.getInstance().Assert(true, "Complexity is missed: "+level_id);
            }
        }

    }

    public static class GMap
    {
        private enum MapSpeed{
            SPEED_0, SPEED_1, CUSTOM
        }
        private enum InjectionId {
            PUSH
        }
        private static class MapSpeedInfo{
            public MapSpeed speedType;
            public MapSpeedInfo(MapSpeed type)
            {
                speedType = type;
            }
        }
        private static class CustomMapSpeedInfo extends MapSpeedInfo{
            public PPS speed;
            public CustomMapSpeedInfo(PPS pps_speed) {
                super(MapSpeed.CUSTOM);
                speed = pps_speed;
            }
        }

        private static MapSpeedInfo ownSpeed = null;
        private static MapSpeedInfo injectedSpeed = null;
        private static InjectionId injectedSpeedId = null;

        private static void InitStatics()
        {
            ownSpeed = null;
            injectedSpeed = null;
        }

        private static void SetSpeed(MapSpeedInfo ownMapSpeed)
        {
            ownSpeed = ownMapSpeed;
        }

        private static void InjectSpeed(MapSpeedInfo injection, InjectionId id)
        {
            injectedSpeed = injection;
            injectedSpeedId = id;
        }

        private static void WithdrawInjectedSpeed(InjectionId id)
        {
            if(id==injectedSpeedId) {
                injectedSpeed = null;
                injectedSpeedId = null;
            }
        }

        public synchronized static float GetTransition()
        {
            MapSpeedInfo info = injectedSpeed==null ? ownSpeed : injectedSpeed;

            if(info==null)
                App.getInstance().Assert(false, "Both Gmap speeds are NULL!");

            switch (info.speedType)
            {
                case SPEED_0:        return 0;
                case SPEED_1:        return PPS.CalcPerFrame(CCountW.Calc(1));
                case CUSTOM:         return ((CustomMapSpeedInfo)info).speed.CalcPerFrame();
                default:
                    App.getInstance().Assert(false, "Speed constant is missed?");
                    return 0;
            }
        }
    }

}
