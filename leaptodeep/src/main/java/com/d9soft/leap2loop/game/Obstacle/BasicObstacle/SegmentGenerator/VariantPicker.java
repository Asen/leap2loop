package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.BuildConfig;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.util.CustomRandom;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class VariantPicker {

    private static int lastBlockVariant=-1, similarBlockCount=0;

    /*
    *
    * Filling segment`s objects with concrete Obstacles.
    * "variantsClass" MUST contain AT LEAST 2 different blocks!
    *
    * */
    public static void PickSegment(Class variantsClass, Collection<BasicObstacle> box)
    {
        App.getInstance().Assert(variantsClass!=null, "Seems that complexity wasnt peeked!");

        //variantsClass = Segment_0.class;
        int variantsNumber = variantsClass.getDeclaredMethods().length;
        int blockVariant = CustomRandom.FromInterval(0, variantsNumber-1);
        if(similarBlockCount == 1 && blockVariant == lastBlockVariant)
            do {
                blockVariant = CustomRandom.FromInterval(0, variantsNumber - 1);
            }while(blockVariant == lastBlockVariant);

        similarBlockCount = lastBlockVariant==blockVariant ? similarBlockCount+1 : 0;
        lastBlockVariant = blockVariant;


        try {

            //blockVariant=4;
            variantsClass.getMethod("V"+blockVariant, Collection.class ).invoke(null, box);

        } catch (Exception e) {
            App.getInstance().Log("Method not found and wasnt called: "+("V"+blockVariant));
        }
    }

    /*
    *
    * Using certain Segment classes allowed only if
    * necessary level was achieved.
    * Here we pick Segment Class depending on current game level:
    *
    * */
    public static Class PickSegmentClass(final Game.ComplexityLevel complexity)
    {
        switch (complexity)
        {
            case Level_Learning_MapPassive:
                return FillForLearningMode0();
            case Level_Learning_MapActive:
                return FillForLearningMode();
            case LVL_0:
                return FillForLevel_0();
            case LVL_50:
                return FillForLevel_50();
            case LVL_100:
                return FillForLevel_100();
            case LVL_150:
                return FillForLevel_150();
            case LVL_200:
                return FillForLevel_200();
            case LVL_250:
                return FillForLevel_250();
            case LVL_300:
                return FillForLevel_300();
        }

        App.getInstance().Assert(true, "Seems that segment-class wasnt peeked!");
        return null;

    }

    private static Class _PickProbableSegment(final HashMap<Class, Integer> chances)
    {
        if(BuildConfig.DEBUG)
        {
            int S = 0;
            for(Integer chance : chances.values()) S+=chance;
            App.getInstance().Assert(S==100, "Sum probability!=100");
        }

        int seed = CustomRandom.FromInterval(1, 100);
        int limit = 0;

        for(Map.Entry<Class, Integer> entry : chances.entrySet()) {
            limit += entry.getValue();
            if (seed <= limit) {
                App.getInstance().Assert(entry.getKey()!=null, "HashMap value is NULL.");
                return entry.getKey();
            }
        }

        App.getInstance().Assert(true, "Probability is broken!");
        return null;
    }

    /*  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<   LEVELS DEFINITIONS  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  */

    private static Class FillForLearningMode0()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_Learning.class, 100);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLearningMode()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 100);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLevel_0()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 100);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLevel_50()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 80);
        probTable.put(Segment_1.class, 20);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLevel_100()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 50);
        probTable.put(Segment_1.class, 40);
        probTable.put(Segment_2.class, 10);
        return _PickProbableSegment(probTable);

    }

    private static Class FillForLevel_150()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 30);
        probTable.put(Segment_1.class, 50);
        probTable.put(Segment_2.class, 20);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLevel_200()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 10);
        probTable.put(Segment_1.class, 50);
        probTable.put(Segment_2.class, 30);
        probTable.put(Segment_3.class, 10);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLevel_250()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 10);
        probTable.put(Segment_1.class, 20);
        probTable.put(Segment_2.class, 40);
        probTable.put(Segment_3.class, 30);
        return _PickProbableSegment(probTable);
    }

    private static Class FillForLevel_300()
    {
        HashMap<Class, Integer> probTable = new HashMap<>();
        probTable.put(Segment_0.class, 10);
        probTable.put(Segment_1.class, 10);
        probTable.put(Segment_2.class, 30);
        probTable.put(Segment_3.class, 50);
        return _PickProbableSegment(probTable);
    }


}
