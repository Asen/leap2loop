package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.d9soft.leap2loop.util.RLoader;

public class PlatformZeroObstacle extends PlatformObstacle {

    protected static final Bitmap obstacleTexture = RLoader.Graphics.Obstacle.Platform.GetStaticPlatform();

    public PlatformZeroObstacle(Customization p_customization) {
        super(p_customization);
        this.Value=0;
    }

    @Override
    protected void _createDrawable() {
        super._createDrawable();

        int width = (int)length;
        int height = (int) this.height;

        drawableObstacle = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(drawableObstacle);
        Rect initialTextureRect=new Rect(0,0,obstacleTexture.getWidth(), obstacleTexture.getHeight());
        canvas.drawBitmap(
                obstacleTexture,
                initialTextureRect,
                new Rect(0, 0, width, height),
                new Paint(Paint.FILTER_BITMAP_FLAG)
        );

    }

}
