package com.d9soft.leap2loop.game.GameEntities;

import android.graphics.Canvas;

import com.d9soft.leap2loop.App;

public abstract class GameEntity {

    /* Standard methods */
    public abstract void PrepareForDrawing();
    public abstract void Draw(Canvas canvas);
    // Each entity of the game should move together with map.
    public abstract void CommonMapMove();
    public void Init(){
        return;
    }

    /* main constructor */
    public GameEntity()
    {
        App.getInstance().Log("New GameEntity has been created");
    }

}
