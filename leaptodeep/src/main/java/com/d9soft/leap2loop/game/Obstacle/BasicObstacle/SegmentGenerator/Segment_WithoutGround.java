package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;


import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformZeroObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;

import java.util.Collection;

public class Segment_WithoutGround {

    /*
    * **********************************************************************************************
    */

    public static void V0(Collection<BasicObstacle> box)
    {

        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.1f), new ScrPartH(0.15f)).
                                setForwardDistance(new CCountW(2))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.1f), new ScrPartH(0.15f)).
                                setForwardDistance(new CCountW(2))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.1f), new ScrPartH(0.15f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V1(Collection<BasicObstacle> box)
    {

        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.1f), new ScrPartH(0.15f)).
                                setForwardDistance(new CCountW(2))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.15f), new ScrPartH(0.45f)).
                                setForwardDistance(new CCountW(2))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.1f), new ScrPartH(0.15f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

}
