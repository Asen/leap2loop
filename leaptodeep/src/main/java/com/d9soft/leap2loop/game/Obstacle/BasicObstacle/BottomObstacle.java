package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;

import android.graphics.RectF;

import com.d9soft.leap2loop.game.L2LConfig;

import java.util.ArrayList;
import java.util.Arrays;

public class BottomObstacle extends DoubleObstacle {

    public BottomObstacle(Customization p_customization) {
        super(p_customization);

        //_setObstacleType(ObstacleType.OBSTACLE_TYPE_BOTTOM);
        freeSpace_startY=0;
        _GeometryChanged();

    }

    @Override
    protected ArrayList<RectF> getGeometryBuffer() {

        if(!geometryBuffer.isEmpty())
            return geometryBuffer;

        RectF[] bGeometry = new RectF[4];
        float bHeight = L2LConfig.GetSpaceHeight() - freeSpace_endY;
        bGeometry[0] = new RectF(leftOffset+length*0.5f, freeSpace_endY, leftOffset+length*0.57f, freeSpace_endY+bHeight*0.2f);
        bGeometry[1] = new RectF(leftOffset + length*0.34f, bGeometry[0].bottom, leftOffset + length*0.7f, freeSpace_endY+bHeight*0.4f);
        bGeometry[2] = new RectF(leftOffset + length*0.2f, bGeometry[1].bottom, leftOffset + length*0.78f, freeSpace_endY+bHeight*0.68f);
        bGeometry[3] = new RectF(leftOffset, bGeometry[2].bottom, leftOffset + length, freeSpace_endY+bHeight);

        geometryBuffer = new ArrayList<>();
        geometryBuffer.addAll(Arrays.asList(bGeometry));
        return geometryBuffer;
    }

    @Override
    public void ReactOnLanding(PlatformObstacle obstacle) {

        boolean isVisible = leftOffset <= L2LConfig.GetSpaceWidth();
        boolean isPlatformObstacleAhead = obstacle.getLeftOffset() > this.getLeftOffset();

        if(!isVisible || isPlatformObstacleAhead)
            return;

        freeSpace_startY = 0;
        freeSpace_endY += obstacle.getValue();

        if(freeSpace_endY<0)
        {
            freeSpace_endY -= obstacle.getValue();
        }else{
            _GeometryChanged();
        }

    }
}
