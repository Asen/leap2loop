package com.d9soft.leap2loop.game.GameEntities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.util.RLoader;

public class Texts extends GameEntity {

    Game gameInstance = null;

    public Texts(Game instance)
    {
        gameInstance = instance;
        Init();
    }

    @Override
    public void Init() {
        super.Init();
    }

    @Override
    public void CommonMapMove() {
        return;
    }

    @Override
    public void PrepareForDrawing() {
        return;
    }

    @Override
    public void Draw(Canvas canvas) {

        final DisplayMetrics display = App.getInstance().getDisplayMetrics();
        final int textSize = display.heightPixels/12;

        Paint p = new Paint();
        p.setColor(Color.CYAN);
        p.setTextSize(textSize);
        p.setFakeBoldText(true);

        if(gameInstance.isGamePaused() && !gameInstance.isGameLost())
        {
            Bitmap pause = RLoader.Graphics.Common.GetGamePause();
            int size = Math.min(pause.getHeight()*2, display.heightPixels/2);
            pause = Bitmap.createScaledBitmap(pause, size, size, false);
            int x = display.widthPixels/2 - pause.getWidth()/2;
            int y = display.heightPixels/2 - pause.getHeight()/2;
            Paint pausePaint = new Paint();
            pausePaint.setAlpha(100);
            canvas.drawBitmap(pause, x, y, pausePaint);
        }

    }
}
