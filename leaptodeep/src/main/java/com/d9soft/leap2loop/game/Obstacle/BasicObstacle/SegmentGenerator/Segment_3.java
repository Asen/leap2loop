package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;

import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.DoubleObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformLoweredObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountH;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;

import java.util.Collection;

public class Segment_3 {

    public static void V0(Collection<BasicObstacle> box)
    {

        box.add(new PlatformLoweredObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.3f), new ScrPartH(0.4f)).
                                setValue(new ScrPartH(-0.075f), new ScrPartH(-0.055f)).
                                setForwardDistance(new CCountW(0.5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.0f)).
                                setBottomPartHeight(new ScrPartH(0.15f), new ScrPartH(0.2f)).
                                setForwardDistance(new CCountW(2f))
                )
        );
        box.add(new PlatformLoweredObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.4f), new ScrPartH(0.5f)).
                                setValue(new ScrPartH(0.1f), new ScrPartH(0.15f)).
                                setForwardDistance(new CCountW(2f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setTopPartHeight(new ScrPartH(0.25f), new ScrPartH(0.45f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V1(Collection<BasicObstacle> box)
    {
        box.add(new PlatformLoweredObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.4f), new ScrPartH(0.45f)).
                                setValue(new ScrPartH(0.025f), new ScrPartH(0.055f)).
                                setForwardDistance(new CCountW(1.5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.0f)).
                                setBottomPartHeight(new ScrPartH(0.45f), new ScrPartH(0.7f)).
                                setForwardDistance(new CCountW(4f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(1.8f)).
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.3f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V2(Collection<BasicObstacle> box)
    {
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.35f)).
                                setValue(new ScrPartH(-0.05f), new ScrPartH(-0.03f)).
                                setForwardDistance(new CCountW(1.0f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.0f)).
                                setBottomPartHeight(new ScrPartH(0.3f), new ScrPartH(0.35f)).
                                setForwardDistance(new CCountW(5f))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.4f), new ScrPartH(0.5f)).
                                setValue(new ScrPartH(0.03f), new ScrPartH(0.06f)).
                                setForwardDistance(new CCountW(0f))
                )
        );
        box.add(new PlatformLoweredObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.35f)).
                                setValue(new ScrPartH(-0.06f), new ScrPartH(-0.03f)).
                                setForwardDistance(new CCountW(1.5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.08f), new ScrPartH(0.15f))
                )
        );
    }

}
