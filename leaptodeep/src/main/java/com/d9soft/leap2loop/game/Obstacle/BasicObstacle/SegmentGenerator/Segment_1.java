package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;

import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BottomObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.DoubleObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformZeroObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountH;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;

import java.util.Collection;

public class Segment_1 {

    public static void V0(Collection<BasicObstacle> box)
    {

        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.2f), new ScrPartH(0.45f)).
                                setValue(new ScrPartH(-0.05f), new ScrPartH(0.05f)).
                                setForwardDistance(new CCountW(3.5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.1f), new ScrPartH(0.3f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V1(Collection<BasicObstacle> box)
    {
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.2f), new ScrPartH(0.35f)).
                                setFreeSpaceHeight(new CCountH(2.0f)).
                                setForwardDistance(new CCountW(3.5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.32f), new ScrPartH(0.45f)).
                                setFreeSpaceHeight(new CCountH(2.5f))
                )
        );
    }

    public static void V2(Collection<BasicObstacle> box)
    {
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setForwardDistance(new CCountW(1f)).
                                setHeight(new ScrPartH(0.4f), new ScrPartH(0.45f)).
                                setValue(new ScrPartH(-0.06f), new ScrPartH(-0.04f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.1f), new ScrPartH(0.2f)).
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setForwardDistance(new CCountW(2f))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setForwardDistance(new CCountW(3f)).
                                setHeight(new ScrPartH(0.5f), new ScrPartH(0.55f)).
                                setValue(new ScrPartH(0.04f), new ScrPartH(0.06f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.2f), new ScrPartH(0.3f)).
                                setFreeSpaceHeight(new CCountH(2.5f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V3(Collection<BasicObstacle> box)
    {
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setForwardDistance(new CCountW(2f)).
                                setHeight(new ScrPartH(0.35f), new ScrPartH(0.45f)).
                                setValue(new ScrPartH(0.05f), new ScrPartH(0.07f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.48f), new ScrPartH(0.52f)).
                                setFreeSpaceHeight(new CCountH(2.f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setForwardDistance(new CCountW(1f)).
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.3f)).
                                setValue(new ScrPartH(-0.075f), new ScrPartH(-0.05f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.2f), new ScrPartH(0.25f)).
                                setFreeSpaceHeight(new CCountH(2.5f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V4(Collection<BasicObstacle> box)
    {

        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.2f), new ScrPartH(0.45f)).
                                setValue(new ScrPartH(0.07f), new ScrPartH(0.14f)).
                                setForwardDistance(new CCountW(2.5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setTopPartHeight(new ScrPartH(0.0f), new ScrPartH(0.15f))
                )
        );
    }

    // ?
    public static void V5(Collection<BasicObstacle> box)
    {

        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.35f), new ScrPartH(0.4f)).
                                setValue(new ScrPartH(-0.03f), new ScrPartH(-0.02f)).
                                setForwardDistance(new CCountW(0.6f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.18f), new ScrPartH(0.19f)).
                                setForwardDistance(new CCountW(0.4f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.27f), new ScrPartH(0.28f)).
                                setForwardDistance(new CCountW(0.1f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.09f), new ScrPartH(0.1f)).
                                setForwardDistance(new CCountW(1.5f))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.3f), new ScrPartH(0.32f)).
                                setForwardDistance(new CCountW(1.25f))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.38f), new ScrPartH(0.47f))
                )
        );
    }

}
