package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;

import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BottomObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.DoubleObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformZeroObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.UpperObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountH;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;

import java.util.Collection;

public class Segment_2 {

    public static void V0(Collection<BasicObstacle> box)
    {

        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.3f), new ScrPartH(0.35f)).
                                setValue(new ScrPartH(-0.1f), new ScrPartH(-0.05f)).
                                setForwardDistance(new CCountW(1.75f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.15f), new ScrPartH(0.17f)).
                                setForwardDistance(new CCountW(0))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.08f), new ScrPartH(0.09f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V1(Collection<BasicObstacle> box)
    {
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.3f), new ScrPartH(0.4f)).
                                setValue(new ScrPartH(-0.12f), new ScrPartH(-0.07f)).
                                setForwardDistance(new CCountW(0.75f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.0f), new ScrPartH(0.0f)).
                                setForwardDistance(new CCountW(1f)).
                                setFreeSpaceHeight(new CCountH(2.5f))
                )
        );
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.45f), new ScrPartH(0.55f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V2(Collection<BasicObstacle> box)
    {
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.3f), new ScrPartH(0.4f)).
                                setValue(new ScrPartH(0.015f), new ScrPartH(0.025f)).
                                setForwardDistance(new CCountW(3f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0f), new ScrPartH(0f)).
                                setForwardDistance(new CCountW(2f)).
                                setFreeSpaceHeight(new CCountH(2.5f))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.45f), new ScrPartH(0.5f)).
                                setValue(new ScrPartH(0f), new ScrPartH(0f))
                )
        );
    }

    /*
    * **********************************************************************************************
    */

    public static void V3(Collection<BasicObstacle> box)
    {
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.42f)).
                                setValue(new ScrPartH(-0.08f), new ScrPartH(0f)).
                                setForwardDistance(new CCountW(1.3f))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.25f), new ScrPartH(0.42f)).
                                setValue(new ScrPartH(0f), new ScrPartH(0.08f)).
                                setForwardDistance(new CCountW(2f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.2f), new ScrPartH(0.3f)).
                                setFreeSpaceHeight(new CCountH(2.0f))
                )
        );
    }

    public static void V4(Collection<BasicObstacle> box)
    {
        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.5f), new ScrPartH(0.55f)).
                                setForwardDistance(new CCountW(0.1f))
                )
        );
        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.35f), new ScrPartH(0.4f)).
                                setForwardDistance(new CCountW(0))
                )
        );
        box.add(new PlatformObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.2f), new ScrPartH(0.3f)).
                                setValue(new ScrPartH(-0.1f), new ScrPartH(-0.05f)).
                                setForwardDistance(new CCountW(0))
                )
        );
        box.add(new BottomObstacle(
                        new DoubleObstacle.Customization().
                                setBottomPartHeight(new ScrPartH(0.25f), new ScrPartH(0.35f)).
                                setForwardDistance(new CCountW(1.15f))
                )
        );
        box.add(new UpperObstacle(
                        new DoubleObstacle.Customization().
                                setTopPartHeight(new ScrPartH(0.69f), new ScrPartH(0.7f))
                )
        );
    }

}
