package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;

import android.graphics.RectF;

import com.d9soft.leap2loop.game.L2LConfig;

import java.util.ArrayList;
import java.util.Arrays;

public class UpperObstacle extends DoubleObstacle {

    public UpperObstacle(Customization p_customization) {
        super(p_customization);

        //_setObstacleType(ObstacleType.OBSTACLE_TYPE_UPPER);
        freeSpace_endY=L2LConfig.GetSpaceHeight();
        _GeometryChanged();

    }

    @Override
    protected ArrayList<RectF> getGeometryBuffer() {

        if(!geometryBuffer.isEmpty())
            return geometryBuffer;

        RectF[] tGeometry = new RectF[4];
        float tHeight = freeSpace_startY;
        tGeometry[0] = new RectF(leftOffset, topOffset, leftOffset + length, topOffset + tHeight*0.32f);
        tGeometry[1] = new RectF(leftOffset + length*0.2f, tGeometry[0].bottom, leftOffset + length*0.78f, topOffset+tHeight*0.68f);
        tGeometry[2] = new RectF(leftOffset + length*0.34f, tGeometry[1].bottom, leftOffset + length*0.7f, topOffset+tHeight*0.8f);
        tGeometry[3] = new RectF(leftOffset + length*0.5f, tGeometry[2].bottom, leftOffset + length*0.57f, topOffset+tHeight);

        geometryBuffer = new ArrayList<>();
        geometryBuffer.addAll(Arrays.asList(tGeometry));
        return geometryBuffer;
    }

    @Override
    public void ReactOnLanding(PlatformObstacle obstacle) {

        boolean isVisible = leftOffset <= L2LConfig.GetSpaceWidth();
        boolean isPlatformObstacleFurther = obstacle.getLeftOffset() > this.getLeftOffset();

        if(!isVisible || isPlatformObstacleFurther)
            return;

        freeSpace_startY += obstacle.getValue();
        freeSpace_endY = L2LConfig.GetSpaceHeight();

        if(freeSpace_startY<0)
        {
            freeSpace_endY -= obstacle.getValue();
        }else{
            _GeometryChanged();
        }

    }
}
