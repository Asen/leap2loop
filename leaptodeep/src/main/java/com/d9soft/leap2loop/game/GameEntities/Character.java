package com.d9soft.leap2loop.game.GameEntities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.DisplayMetrics;
import android.util.Log;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.RectTransitionVector;
import com.d9soft.leap2loop.util.RLoader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class Character extends GameEntity{

    public static final float CHARACTER_WIDTH_FACTOR = 516f / 900f;
    public static final float CHARACTER_PHYSIC_WIDTH = 0.56f;
    public static final float CHARACTER_PHYSIC_HEIGHT = 0.8f;

    private enum CharacterDirectionY{ UP, DOWN }
    private enum CharacterGravity{ ON, OFF }
    private enum CharacterAnimSprite{ NORMAL, SHRUNK, EXTENDED }
    private enum CharacterState{UNDEFINED, GROUND, ON_TOP}
    /////////////////////////////////////////////////////////////////////////////
    private static final Bitmap BMP_NORMAL = RLoader.Graphics.Character.GetDefault();
    private static final Bitmap BMP_SHRUNK = RLoader.Graphics.Character.GetShrunk();
    /////////////////////////////////////////////////////////////////////////////
    private float currentAccelerationY = 1.0f;
    private float currentAccelerationX = 0f;
    private float ACCELERATION_OY_UP;
    private float ACCELERATION_OY_DOWN;
    private float ACCELERATION_OY_GRAVITATIONOFF;
    private float ACCELERATION_OX_GRAVITATIONOFF;
    private float BASE_SPEED_OY_UP;
    private float BASE_SPEED_OY_DOWN;
    private float MIN_X_POS;
    /////////////////////////////////////////////////////////////////////////////
    private PointF bottomrightCoords = null;
    private RectTransitionVector transitionVec = new RectTransitionVector();
    private float gonePixels = 0f;
    private float speedXperFrame = 0f;
    private float width;
    private float height;
    private float lastLandedY = L2LConfig.GetGroundLevel();
    private LandingZone landingZone = new LandingZone();
    private LearningInterface learningInterface = null;
    private HashMap<CharacterAnimSprite, Bitmap> sprites = new HashMap<>();
    /////////////////////////////////////////////////////////////////////////////
    private boolean __isUserManageable = true;
    /////////////////////////////////////////////////////////////////////////////
    private CharacterGravity gravity = CharacterGravity.ON;
    private CharacterDirectionY characterDirectionY = CharacterDirectionY.UP;
    private CharacterAnimSprite characterSprite = CharacterAnimSprite.NORMAL;
    private CharacterState characterState = Character.CharacterState.UNDEFINED;
    /////////////////////////////////////////////////////////////////////////////
    Game gameInstance;


    /////////////////////////////////////////////////////////////////////////////
    //Character is 30FPS dependent. Here is a little API of fixing it:
    int FPS = 30;
    long framesTotal = 0;
    private void _FPSEvent(){ framesTotal++; }
    private boolean _isFPSTime()
    {
        return framesTotal%(L2LConfig.FPS()/this.FPS) == 0;
    }
    /////////////////////////////////////////////////////////////////////////////

    public Character(Game gameContext)
    {
        gameInstance = gameContext;
        Init();
    }

    private Character trajCharacter = null;
    private final ArrayList<PointF> trajectory = new ArrayList<>();
    public Collection<PointF> CalcTrajectory()
    {
        if(trajCharacter == null)
            trajCharacter = new Character(gameInstance);

        trajCharacter.bottomrightCoords.x = this.bottomrightCoords.x;
        trajCharacter.bottomrightCoords.y = this.bottomrightCoords.y;
        trajCharacter.currentAccelerationX = this.currentAccelerationX;
        trajCharacter.currentAccelerationY = this.currentAccelerationY;
        trajCharacter.characterDirectionY = this.characterDirectionY;
        trajCharacter.Finger();
        float X=0, Y=0;

        trajectory.clear();

        for(int i=0; i<75; ++i)
        {
            if(trajCharacter.gravity == CharacterGravity.ON)
                trajCharacter._DoGravityMotion();
            else
                trajCharacter._DoWithoutGravity();

            /* condition: remove duplicates */
            if(X != trajCharacter.bottomrightCoords.x) {
                X = trajCharacter.bottomrightCoords.x;
                Y = trajCharacter.bottomrightCoords.y;

                trajectory.add(new PointF(X, Y));
                trajectory.add(new PointF(X, Y-height));
            }
        }

        return trajectory;

    }

    public RectF getGraphicGeometry()
    {
        float left =  bottomrightCoords.x-width;
        float top =  bottomrightCoords.y-height;
        float right = bottomrightCoords.x;
        float bottom = bottomrightCoords.y;
        //////////////////////////////////////////
        return new RectF(left, top, right, bottom);
    }

    private RectF _getPhysicGeometry()
    {
        // Division because Character`s body without its tail equals CHARACTER_PHYSIC_WIDTH of
        // entire Character texture. And tail isn`t important.
        float left =  bottomrightCoords.x - width*CHARACTER_PHYSIC_WIDTH;
        float top =  bottomrightCoords.y-height;
        float right = bottomrightCoords.x;
        // When characters up his feet on 0.8 of full height high
        float bottom = top + height*CHARACTER_PHYSIC_HEIGHT;
        //////////////////////////////////////////
        return new RectF(left, top, right, bottom);
    }

    private Bitmap _GetActualSprite()
    {
        switch (characterSprite)
        {
            case SHRUNK:
                return sprites.get(CharacterAnimSprite.SHRUNK);
            case EXTENDED:
                return sprites.get(CharacterAnimSprite.EXTENDED);
            default:
                return sprites.get(CharacterAnimSprite.NORMAL);
        }
    }

    public boolean IsOnTheMapEdge()
    {
        float MAP_START_SCROLL_POINT = L2LConfig.GetSpaceWidth()*0.6f;
        return this.bottomrightCoords.x >= MAP_START_SCROLL_POINT;
    }

    public float GetCharacterSpeedPerFrame()
    {
        return speedXperFrame;
    }

    private void _Landing_CheckIfCharacterInZone()
    {
        BasicObstacle obstacle = gameInstance.getSurface().getObstacleCharacterOn(this);

        if(obstacle != null) {
            landingZone.Detect(obstacle.getTopOffset(), LandingZone.LandingType.OBSTACLE);
            characterState=CharacterState.ON_TOP;
            return;
        }

        if(isCharacterOnGround()) {
            characterState=CharacterState.GROUND;
            landingZone.Detect(L2LConfig.GetGroundLevel(), LandingZone.LandingType.GROUND);
            return;
        }

        landingZone.Out();
    }

    private void _Landing_FinallyLanded()
    {
        if(landingZone.landingType==LandingZone.LandingType.OBSTACLE) {
            gameInstance.AddScore(1);
            gameInstance.getSurface().ReactOnLanding(this);
            gameInstance.ReportCharacterScore();
        } else{
            gameInstance.ReportCharacterLeap();
        }

    }

    private void _Landing_Routine()
    {
        _Landing_CheckIfCharacterInZone();

        boolean isZoneEndReached = bottomrightCoords.y >= landingZone.surfaceZoneEnd;
        if(landingZone.isInLandingZone && isZoneEndReached) {
            bottomrightCoords.y = landingZone.surfaceZoneEnd;
            lastLandedY = landingZone.surfaceZoneEnd;
            currentAccelerationY = 1f;
            characterDirectionY = CharacterDirectionY.UP;
            characterSprite = CharacterAnimSprite.NORMAL;
            //characterState = isCharacterOnGround() ? CharacterState.GROUND : CharacterState.ON_TOP;

            this._Landing_FinallyLanded();
        }
    }

    private void _DoMotion()
    {
        /* Character motion implemented as 30-iteration cycle */
        _FPSEvent();
        if(!_isFPSTime())
            return;

        ///////////// Calculating X,Y-coordinates only ///////////////////
        if(gravity == CharacterGravity.ON)
            _DoGravityMotion();
         else
            _DoWithoutGravity();

        // Catching Landing-event
        this._Landing_Routine();

    }

    private void _DoGravityMotion()
    {
        /* OY Movement of character */
        if( characterDirectionY == CharacterDirectionY.UP ){
            currentAccelerationX = 0;
            currentAccelerationY += ACCELERATION_OY_UP;
            bottomrightCoords.y += BASE_SPEED_OY_UP * currentAccelerationY;
            __CheckIfSpriteShouldBeShrunk();
            //App.getInstance().Log("[ACCA] " + currentAccelerationY + " ; " + BASE_SPEED_OY_UP * currentAccelerationY);

        } else{
            currentAccelerationY += ACCELERATION_OY_DOWN;
            bottomrightCoords.y += BASE_SPEED_OY_DOWN * currentAccelerationY;
            //App.getInstance().Log("[ACCA] "+currentAccelerationY+" ; "+ BASE_SPEED_OY_DOWN * currentAccelerationY);
        }


        /* OX movement */
        if(currentAccelerationX>0) {

            /* if we have an acceleration OX */
            bottomrightCoords.x += currentAccelerationX;
            currentAccelerationX *= 0.87;
            if (bottomrightCoords.x > L2LConfig.GetSpaceWidth())
                bottomrightCoords.x = L2LConfig.GetSpaceWidth();

        }

        /* Check if upper bound reached */
        boolean isUpperBoundReached = lastLandedY - bottomrightCoords.y >= L2LConfig.GetCharacterLeapHigh();
        if(characterDirectionY==CharacterDirectionY.UP && isUpperBoundReached){
            currentAccelerationY = 0f;
            characterDirectionY = CharacterDirectionY.DOWN;
            characterSprite = CharacterAnimSprite.NORMAL;
        }

    }

    private void _DoWithoutGravity()
    {
        currentAccelerationX += ACCELERATION_OX_GRAVITATIONOFF;
        currentAccelerationY -= ACCELERATION_OY_GRAVITATIONOFF;

        bottomrightCoords.x += currentAccelerationX;
        boolean isCharMovingUp = characterDirectionY == CharacterDirectionY.UP;
        bottomrightCoords.y +=
                isCharMovingUp ?
                        BASE_SPEED_OY_UP * currentAccelerationY * 1.5f :
                        BASE_SPEED_OY_DOWN * currentAccelerationY;

        if (bottomrightCoords.x > L2LConfig.GetSpaceWidth()) {
            bottomrightCoords.x = L2LConfig.GetSpaceWidth();
        }
        if (bottomrightCoords.x <= MIN_X_POS) {
            bottomrightCoords.x = (int) MIN_X_POS;
        }
        if (bottomrightCoords.y < height) {
            bottomrightCoords.y = height;
            NoFinger();
        }
        if (bottomrightCoords.y > L2LConfig.GetGroundLevel()) {
            bottomrightCoords.y = L2LConfig.GetGroundLevel();
        }

        if (isCharacterOnGround())
            NoFinger();

    }

    private void __CheckIfSpriteShouldBeShrunk()
    {
        characterSprite = CharacterAnimSprite.NORMAL;

        // Shrunk when 1/4 of leap height is gone:
        float spaceY = L2LConfig.GetGroundLevel()-L2LConfig.GetCharacterLeapHigh()*0.25f;
        if(bottomrightCoords.y <= spaceY)
            characterSprite = CharacterAnimSprite.SHRUNK;
    }

    public void Finger()
    {
        currentAccelerationX = new CCountW(0.05f).Calc();
        characterSprite = CharacterAnimSprite.SHRUNK;
        gravity = CharacterGravity.OFF;
    }

    public void NoFinger()
    {
        characterSprite = CharacterAnimSprite.NORMAL;
        characterDirectionY = CharacterDirectionY.DOWN;
        //characterSprite = CharacterAnimSprite.SHRUNK;
        gravity = CharacterGravity.ON;
    }

    public CCountW getGoneDistance() { return new CCountW( gonePixels/L2LConfig.GetCharacterWidth() ); }
    public RectTransitionVector getTransition() { return transitionVec; }
    public float getLeftXPos() { return bottomrightCoords.x - width; }
    public void Kill() { gameInstance.Lose(); }
    public void MoveToStartPosition(){ bottomrightCoords.x = MIN_X_POS; }
    public boolean IsFingerOnScreen(){ return gravity==CharacterGravity.OFF; }
    public boolean isCharacterOnTop(){ return characterState==CharacterState.ON_TOP; }
    public boolean isCharacterOnGround(){ return bottomrightCoords.y>=L2LConfig.GetGroundLevel(); }

    public void DisableUserManagement(){
        __isUserManageable = false;
    }
    public void EnableUserManagement(){
        __isUserManageable = true;
    }
    public boolean IsUserManageable(){ return __isUserManageable; }
    public boolean IsInTopOfLeap(){
        boolean isMovingUp=characterDirectionY==CharacterDirectionY.UP;
        return isMovingUp && lastLandedY-bottomrightCoords.y >= L2LConfig.GetCharacterLeapHigh()*0.98f;
    }
    public boolean IsInBottomOfLeap(){
        boolean isMovingUp=characterDirectionY==CharacterDirectionY.UP;
        return isMovingUp && lastLandedY-bottomrightCoords.y <= L2LConfig.GetCharacterLeapHigh()*0.25f;
    }

    public LearningInterface GetLearningInterface()
    {
        if(learningInterface ==null)
            learningInterface = new LearningInterface();
        return learningInterface;
    }

    @Override
    public void Init() {

        super.Init();

        final DisplayMetrics display = App.getInstance().getDisplayMetrics();
        ///////////////////////////////////////////////////////////////////
        ACCELERATION_OY_GRAVITATIONOFF = display.heightPixels / 192000f;
        ACCELERATION_OX_GRAVITATIONOFF = display.widthPixels / 1575f;
        ///////////////////////////////////////////////////////////////////

        /*

        * Character "gravity"-motion performs in 30 steps per second.
        * 18 steps - to upper point;
        * 12 steps - from upper to bottom point
        *
        * */


        /*
        *
        * BASE_SPEED_OY_UP < -LeapHeight/18
        *         &&                          =>   BASE_SPEED_OY_UP  C  (-∞, -LeapHeight/18)
        * BASE_SPEED_OY_UP < 0
        *
        * */
        BASE_SPEED_OY_UP = (-L2LConfig.GetCharacterLeapHigh() / 18f) * 2.f;

        /*
        * Via to "Gravity Motion Function" => 18 Steps :
        * --------------------------------------------------------------------------------------
        * (1 step) Y1 = BaseSpeedUp * (1+AccelerationUp)
        * (2 step) Y2 = BaseSpeedUp * (1+AccelerationUp+AccelerationUp)
        * (3 step) Y3 = BaseSpeedUp * (1+AccelerationUp+AccelerationUp+AccelerationUp)
        * ...
        * (18 step) Y18 = BaseSpeedUp * (1+18*AccelerationUp)
        * --------------------------------------------------------------------------------------
        * Common Transition = LeapHeight = (Sum of all above) = 18*BaseSpeedUp+171*BaseSpeedUp*AccelerationUp
        * 171 = 1+2+3+4+...+18 => Arithmetical progression till 18;
        *
        *
        * -LeapHeight = 18*BASE_SPEED_OY_UP + 171*BASE_SPEED_OY_UP*ACCELERATION_OY_UP;
        *
        *                      -LeapHeight - 18*BASE_SPEED_OY_UP
        * ACCELERATION_OY_UP = ===================================
        *                            171*BASE_SPEED_OY_UP
        *
        * */
        ACCELERATION_OY_UP = (-L2LConfig.GetCharacterLeapHigh() - 18*BASE_SPEED_OY_UP) /  (171f*BASE_SPEED_OY_UP);


        /*
        *
        * Just the smallest possible speed
        *
        * */
        BASE_SPEED_OY_DOWN = 1f;

        /*
        * Via to "Gravity Motion Function" => 12 Steps:
        * --------------------------------------------------------------------------------------
        * (1 step) Y1 = BaseSpeedDown * (1-AccelerationDown)
        * (2 step) Y2 = BaseSpeedDown * (1-AccelerationDown-AccelerationDown)
        * (3 step) Y3 = BaseSpeedDown * (1-AccelerationDown-AccelerationDown-AccelerationDown)
        * ...
        * (12 step) Y18 = BaseSpeedDown * (1-12*AccelerationDown)
        * --------------------------------------------------------------------------------------
        * Common Transition = LeapHeight = (Sum of all above) = 12*BaseSpeedDown+78*BaseSpeedUp*AccelerationUp
        * 78 = 1+2+3+4+...+12 => Arithmetical progression till 12;
        *
        *
        * +LeapHeight = 12*BASE_SPEED_OY_UP + 78*BASE_SPEED_OY_UP*ACCELERATION_OY_UP;
        *
        *                      +LeapHeight - 12*BASE_SPEED_OY_UP
        * ACCELERATION_OY_UP = ===================================
        *                            78*BASE_SPEED_OY_UP
        *
        * !! but...
        * BASE_SPEED_OY_UP is almost = 0( speed in highest point of leap ), SO:
        *
        *                       LeapHeight - 12
        * ACCELERATION_OY_UP = =================
        *                             78
        *
        * */
        ACCELERATION_OY_DOWN = (L2LConfig.GetCharacterLeapHigh()-12) / 78f;

        App.getInstance().Log("[CHAR] ACC_UP: "+ACCELERATION_OY_UP);
        App.getInstance().Log("[CHAR] ACC_DOWN: "+ACCELERATION_OY_DOWN);



        gonePixels = 0;
        height = L2LConfig.GetCharacterHeight();
        width = L2LConfig.GetCharacterWidth();

        MIN_X_POS = width + L2LConfig.GetCharacterXOffset();
        bottomrightCoords = new PointF(MIN_X_POS, 0);

        // InitAnimator sprite animation
        //sprites.put(CharacterAnimSprite.EXTENDED, App.getInstance().decDrawableRes(R.drawable.sprite_extended));
        sprites.put(CharacterAnimSprite.SHRUNK, BMP_SHRUNK);
        sprites.put(CharacterAnimSprite.NORMAL, BMP_NORMAL);

        // Scaling sprites
        RectF chMetrics = this.getGraphicGeometry();
        for(CharacterAnimSprite sprite:sprites.keySet())
            sprites.put(sprite,Bitmap.createScaledBitmap(
                    sprites.get(sprite),
                    (int)(chMetrics.right-chMetrics.left),
                    (int)(chMetrics.bottom-chMetrics.top),
                    true));

    }

    private void _AddGoneDistance(float pxValue)
    {
        gonePixels += pxValue;
    }

    @Override
    public void CommonMapMove() {

        // Parallel motion
        float characterMinX = this.width + L2LConfig.GetCharacterXOffset();
        float distance = Game.GMap.GetTransition();
        if (bottomrightCoords.x > characterMinX)
            bottomrightCoords.x -= distance;
        else
            _AddGoneDistance(distance);

    }

    @Override
    public void PrepareForDrawing() {

        // Movement
        RectF startMetrics = _getPhysicGeometry();
        this._DoMotion();
        this.gameInstance.getSurface().ReportCharacterMotion(this);

        // Learning Routine
        if(learningInterface!=null)
            GetLearningInterface().Tick();

        // Catching the character`s transition-X,Y
        transitionVec.Init(startMetrics, _getPhysicGeometry());

        // Speed measure
        speedXperFrame = transitionVec.GetXTransition();

        // Movement calculation
        if(transitionVec.GetXTransition() > 0)
            _AddGoneDistance(transitionVec.GetXTransition());

    }

    @Override
    public void Draw(Canvas canvas) {

        canvas.drawBitmap(
                this._GetActualSprite(),
                this.getGraphicGeometry().left,
                this.getGraphicGeometry().top,
                null);

    }

    private static class LandingZone{

        private enum LandingType {GROUND, OBSTACLE, UNDEFINED}

        private boolean isInLandingZone = false;
        private float surfaceZoneEnd = 0;
        private LandingType landingType = LandingType.UNDEFINED;

        public void Detect(float zoneEnd, LandingType type)
        {
            isInLandingZone = true;
            surfaceZoneEnd = zoneEnd;
            landingType = type;
        }

        public void Out()
        {
            isInLandingZone = false;
            surfaceZoneEnd = 0;
            landingType = LandingType.UNDEFINED;
        }

    }

    /////////////////////////////////////////////////////////////////
    //  LearningMode Functionality
    ////////////////////////////////////////////////////////////////

    public final class LearningInterface
    {
        private float autoPlayStartPosX = 0;
        private StringBuilder recBuffer = new StringBuilder();
        private int recIndex = 1;
        private boolean preparingForRecording = false,
                        recordingMode = false,
                        recordingComplete = false,
                        preparingForLearning = false,
                        learningMode = false,
                        learningComplete = false;

        public void PrepareForRecording(float _startX)
        {
            if(!recordingMode) {
                preparingForRecording = true;
                currentAccelerationX = 0;
                autoPlayStartPosX = _startX;
                NoFinger();
            }
        }

        public void StopRecordingMode()
        {
            if(recordingMode) {

                recordingMode = false;
                recordingComplete = true;
                gameInstance.Pause();
                NoFinger();

                final int L = 1024;
                for (int i = 0; i < recBuffer.length(); i += L) {
                    int end = i+L < recBuffer.length() ? i+L : recBuffer.length();
                    Log.i("[Rec]", recBuffer.substring(i, end));
                }
            }
        }

        public void PrepareForLearning(float _startX)
        {
            if(!learningMode) {
                preparingForLearning = true;
                currentAccelerationX = 0;
                autoPlayStartPosX = _startX;
                DisableUserManagement();
                NoFinger();
            }
        }

        public void DisableLearning()
        {
            if(learningMode) {
                learningMode = false;
                learningComplete = true;
                EnableUserManagement();
                NoFinger();
                App.getInstance().Log("[DisableLearning]");
            }
        }

        public boolean IsLearningComplete()
        {
            return learningComplete;
        }

        public boolean IsRunning()
        {
            return preparingForLearning || preparingForRecording || learningMode || recordingMode;
        }

        private void Tick()
        {
            // Searching Recording Start Point
            if(preparingForRecording && isCharacterOnGround())
            {
                preparingForRecording = false;
                recordingMode = true;
                App.getInstance().Log("[Rec] Started");
            }

            // Searching Learning Start Point
            if(preparingForLearning && isCharacterOnGround())
            {
                preparingForLearning = false;
                learningMode = true;
                App.getInstance().Log("[AutoPlay] Started");
            }

            if(recordingMode) RecordFrame();
            if(learningMode) PlayFrame();

            // Point transition, where learning started
            GetLearningInterface().autoPlayStartPosX -= Game.GMap.GetTransition();
        }

        private void RecordFrame()
        {
            float x = (bottomrightCoords.x - autoPlayStartPosX) / L2LConfig.GetCharacterWidth();
            float y = bottomrightCoords.y / L2LConfig.GetSpaceHeight();
            float acceleration = currentAccelerationY / (characterDirectionY==CharacterDirectionY.UP ? ACCELERATION_OY_UP : ACCELERATION_OY_DOWN);
            char direction = characterDirectionY==CharacterDirectionY.UP ? '1':'0';
            char isFlying = IsFingerOnScreen() ? '1':'0';
            if(x>=0)
                recBuffer.append(x+","+y+","+acceleration+","+direction+","+isFlying+';');
        }

        private void PlayFrame()
        {
            if(recIndex >= RLoader.Data.Learning.GetMotionMap().length)
            {
                DisableLearning();
                return;
            }

            try {

                String[] info = RLoader.Data.Learning.GetMotionMap()[recIndex++].split(",");
                float x = autoPlayStartPosX + L2LConfig.GetCharacterWidth() * Float.parseFloat(info[0]);
                if(x>bottomrightCoords.x) {
                    bottomrightCoords.x = x;
                }
                bottomrightCoords.y = L2LConfig.GetSpaceHeight() * Float.parseFloat(info[1]);
                characterDirectionY = Integer.parseInt(info[3]) == 1 ? CharacterDirectionY.UP : CharacterDirectionY.DOWN;
                currentAccelerationY = Float.parseFloat(info[2]) * (characterDirectionY==CharacterDirectionY.UP ? ACCELERATION_OY_UP : ACCELERATION_OY_DOWN);
                gravity = Integer.parseInt(info[4])==1 ? CharacterGravity.OFF : CharacterGravity.ON;

            }catch (Exception e)
            {
                App.getInstance().Log("[ADATA]"+RLoader.Data.Learning.GetMotionMap()[recIndex-1]);
                App.getInstance().Assert(false, e.getMessage());
            }
        }

    }
}
