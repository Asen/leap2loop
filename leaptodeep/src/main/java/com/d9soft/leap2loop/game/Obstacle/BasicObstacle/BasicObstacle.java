package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;


import android.graphics.Bitmap;
import android.graphics.RectF;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.GameEntities.Character;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;

import java.util.ArrayList;


public abstract class BasicObstacle{

    protected float topOffset;
    protected float leftOffset = -1;
    protected float length;
    protected boolean isActive = false;
    ////////////////////////////////////////////
    protected float freeSpace_startY = 0;
    protected float freeSpace_endY = 0;
    ////////////////////////////////////////////
    protected ArrayList<RectF> geometryBuffer = new ArrayList<>();
    private boolean isGeometryChanged = true;
    final protected void _GeometryChanged() {
        isGeometryChanged = true;
        geometryBuffer.clear();
    }
    protected void _createDrawable(){
        isGeometryChanged = false;
    }
    public Bitmap getDrawable(){
        if(isGeometryChanged)
            _createDrawable();
        return null;
    }
    ////////////////////////////////////////////
    public abstract boolean isCharacterOnSurface(Character character);
    public abstract void ReactOnLanding(PlatformObstacle obstacle);
    protected abstract ArrayList<RectF> getGeometryBuffer();
    public boolean isCharacterIntersected(Character character)
    {
        for(RectF intersectionMetrics : getGeometryBuffer())
        {
            boolean isIntersection = character.getTransition().isIntersected(intersectionMetrics);
            if(isIntersection)
                return true;
        }
        return false;
    }
    ////////////////////////////////////////////
    final public void setLeftOffset(float offset){ leftOffset = Math.abs(offset); }
    final public void parallelMove()
    {
        // moving each obstacle according to current map speed
        leftOffset -= Game.GMap.GetTransition();

        // moving obstacle`s geometry according to current map speed
        for(int i=0; i< geometryBuffer.size(); ++i)
        {
            geometryBuffer.get(i).left -= Game.GMap.GetTransition();
            geometryBuffer.get(i).right -= Game.GMap.GetTransition();
        }
    }
    ////////////////////////////////////////////
    final public float getLeftOffset(){
        if(leftOffset==-1)
            App.getInstance().Assert(false, "LeftOffsset isnt defined yet!");
        return leftOffset;
    }
    final public float getTopOffset(){ return topOffset; }
    final public float getLength(){ return length; }
    ////////////////////////////////////////////
    protected int preferableForwardDistance = (int) L2LConfig.GetDefaultObstacleBetweenDistance().Calc();
    public int getForwardDistance(){ return preferableForwardDistance; }
    protected BasicObstacle _pickForwardDistance(CCountW factor){
        if(factor==null)
            return this;
        preferableForwardDistance = (int)factor.Calc();
        return this;
    }
}
