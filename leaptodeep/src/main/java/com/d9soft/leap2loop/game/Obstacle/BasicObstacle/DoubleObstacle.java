package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.GameEntities.Character;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.util.CustomRandom;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountH;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;
import com.d9soft.leap2loop.util.RLoader;

import java.util.ArrayList;
import java.util.Arrays;


public class DoubleObstacle extends BasicObstacle {

    private static final Bitmap topTexture = RLoader.Graphics.Obstacle.Double.GetTop();
    private static final Bitmap bottomTexture = RLoader.Graphics.Obstacle.Double.GetBottom();

    private Bitmap drawableObstacle = null;
    protected float freeSpaceHeight = 0;

    public DoubleObstacle(DoubleObstacle.Customization p_customization)
    {
        _InitGeometry(p_customization);
        _createDrawable();
    }

    private void _InitGeometry(DoubleObstacle.Customization p_customization)
    {
        _pickFreeSpaceHeight(p_customization.freeSpaceHeight);
        _pickTopPartHeight(p_customization.minTopPartHeight, p_customization.maxTopPartHeight);
        _pickBottomPartHeight(p_customization.minBottomPartHeight, p_customization.maxBottomPartHeight);
        _pickForwardDistance(p_customization.forwardDistance);
        _pickWidth(p_customization.minWidth, p_customization.maxWidth);

        topOffset = 0;

        App.getInstance().Log("New DoubleObstacle created(width=" + length + "; freespaceStart=" + freeSpace_startY + ")");
    }

    @Override
    public void ReactOnLanding(PlatformObstacle obstacle) {

        boolean isVisible = leftOffset<=L2LConfig.GetSpaceWidth();
        boolean isPlatformObstacleAhead = obstacle.getLeftOffset() > this.getLeftOffset();

        if(!isVisible || isPlatformObstacleAhead)
            return;

        int obstacleScore = obstacle.getValue();
        freeSpace_startY += obstacleScore;
        freeSpace_endY += obstacleScore;

        if(freeSpace_endY<0 || freeSpace_startY>L2LConfig.GetSpaceHeight())
        {
            freeSpace_startY -= obstacleScore;
            freeSpace_endY -= obstacleScore;
        }else{
            _GeometryChanged();
        }

    }

    @Override
    protected ArrayList<RectF> getGeometryBuffer() {

        if(!geometryBuffer.isEmpty())
            return geometryBuffer;

        /*
        Bottom part of this obstacle is 4 rectangles inscribed in bottom triangle shape
        */
        RectF[] bGeometry = new RectF[4];
        float bHeight = L2LConfig.GetSpaceHeight() - freeSpace_endY;
        bGeometry[0] = new RectF(leftOffset+length*0.5f, freeSpace_endY, leftOffset+length*0.57f, freeSpace_endY+bHeight*0.2f);
        bGeometry[1] = new RectF(leftOffset + length*0.34f, bGeometry[0].bottom, leftOffset + length*0.7f, freeSpace_endY+bHeight*0.4f);
        bGeometry[2] = new RectF(leftOffset + length*0.2f, bGeometry[1].bottom, leftOffset + length*0.78f, freeSpace_endY+bHeight*0.68f);
        bGeometry[3] = new RectF(leftOffset, bGeometry[2].bottom, leftOffset + length, freeSpace_endY+bHeight);

        /*
        Top part of this obstacle is also 4 rectangles inscribed in upper triangle shape
        */
        RectF[] tGeometry = new RectF[4];
        float tHeight = freeSpace_startY;
        tGeometry[0] = new RectF(leftOffset, topOffset, leftOffset + length, topOffset + tHeight*0.32f);
        tGeometry[1] = new RectF(leftOffset + length*0.2f, tGeometry[0].bottom, leftOffset + length*0.78f, topOffset+tHeight*0.68f);
        tGeometry[2] = new RectF(leftOffset + length*0.34f, tGeometry[1].bottom, leftOffset + length*0.7f, topOffset+tHeight*0.8f);
        tGeometry[3] = new RectF(leftOffset + length*0.5f, tGeometry[2].bottom, leftOffset + length*0.57f, topOffset+tHeight);

        geometryBuffer.addAll(Arrays.asList(bGeometry));
        geometryBuffer.addAll(Arrays.asList(tGeometry));
        return geometryBuffer;
    }

    @Override
    public boolean isCharacterOnSurface(Character character) {
        return false;
    }

    @Override
    public Bitmap getDrawable() {
        super.getDrawable();
        return drawableObstacle;
    }

    @Override
    protected void _createDrawable() {
        super._createDrawable();

        int width = (int)length;
        int height = L2LConfig.GetSpaceHeight();
        drawableObstacle = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(drawableObstacle);

        Rect initialTopTextureRect = new Rect(0, 0, topTexture.getWidth(), topTexture.getHeight());
        if(freeSpace_startY>0)
            canvas.drawBitmap(
                    topTexture,
                    initialTopTextureRect,
                    new Rect(0, 0, width, (int)freeSpace_startY),
                    new Paint(Paint.FILTER_BITMAP_FLAG)
            );

        Rect initialBottomTextureRect = new Rect(0, 0, bottomTexture.getWidth(), bottomTexture.getHeight());
        if(freeSpace_endY<L2LConfig.GetSpaceHeight())
            canvas.drawBitmap(
                    bottomTexture,
                    initialBottomTextureRect,
                    new Rect(0, (int) freeSpace_endY, width, height),
                    new Paint(Paint.FILTER_BITMAP_FLAG)
            );

    }



    /*
    **********************************************************
    ******************* Customization ************************
    **********************************************************
     */

    public static class Customization {
        ScrPartH minTopPartHeight = null,
                 maxTopPartHeight = null,
                 minBottomPartHeight = null,
                 maxBottomPartHeight = null;
        CCountW minWidth = null,
                maxWidth = null;
        CCountH freeSpaceHeight = null;
        CCountW forwardDistance = null;

        public Customization setTopPartHeight(ScrPartH p_minStart, ScrPartH p_maxStart) {
            minTopPartHeight = new ScrPartH(p_minStart);
            maxTopPartHeight = new ScrPartH(p_maxStart);
            return this;
        }

        public Customization setBottomPartHeight(ScrPartH p_minEnd, ScrPartH p_maxEnd) {
            minBottomPartHeight = new ScrPartH(p_minEnd);
            maxBottomPartHeight = new ScrPartH(p_maxEnd);
            return this;
        }

        public Customization setWidth(CCountW p_minW, CCountW p_maxW) {
            minWidth = new CCountW(p_minW);
            maxWidth = new CCountW(p_maxW);
            return this;
        }

        public Customization setFreeSpaceHeight(CCountH p_height) {
            freeSpaceHeight = new CCountH(p_height);
            return this;
        }

        public Customization setForwardDistance(CCountW p_fwdDistance) {
            forwardDistance = new CCountW(p_fwdDistance);
            return this;
        }
    }

    protected DoubleObstacle _pickTopPartHeight(ScrPartH p_minStart, ScrPartH p_maxStart)
    {
        if(p_minStart==null || p_maxStart==null)
            return this;

        int minStartPoint = (int) p_minStart.Calc();
        int maxStartPoint = (int) p_maxStart.Calc();

        if(minStartPoint > maxStartPoint)
            return this;

        freeSpace_startY = CustomRandom.FromInterval(minStartPoint, maxStartPoint);
        if(freeSpace_startY > (L2LConfig.GetSpaceHeight() - freeSpaceHeight))
            freeSpace_startY = L2LConfig.GetSpaceHeight() - freeSpaceHeight;
        freeSpace_endY = freeSpace_startY + freeSpaceHeight;

        _GeometryChanged();
        return this;
    }

    protected DoubleObstacle _pickBottomPartHeight(ScrPartH p_minEnd, ScrPartH p_maxEnd)
    {
        if(p_minEnd==null || p_maxEnd==null)
            return this;

        int minEndPoint = (int) p_minEnd.Calc();
        int maxEndPoint = (int) p_maxEnd.Calc();

        if(minEndPoint > maxEndPoint)
            return this;

        freeSpace_endY = L2LConfig.GetSpaceHeight() - CustomRandom.FromInterval(minEndPoint, maxEndPoint);
        if(freeSpace_endY < freeSpaceHeight)
            freeSpace_endY = freeSpaceHeight;
        freeSpace_startY = freeSpace_endY - freeSpaceHeight;

        _GeometryChanged();
        return this;
    }

    protected DoubleObstacle _pickWidth(CCountW p_minW, CCountW p_maxW)
    {
        int minWidth = (int) (p_minW==null ? new CCountW(0.65f).Calc() : p_minW.Calc());
        int maxWidth = (int) (p_maxW==null ? new CCountW(1.55f).Calc() : p_maxW.Calc());

        if(minWidth > maxWidth)
            return this;

        length = CustomRandom.FromInterval(minWidth, maxWidth);
        _GeometryChanged();

        return this;
    }

    protected DoubleObstacle _pickFreeSpaceHeight(CCountH p_height)
    {
        if(p_height==null)
            return this;

        freeSpaceHeight = (int) p_height.Calc();
        freeSpace_endY = freeSpace_startY + freeSpaceHeight;

        return this;
    }

    @Override
    protected BasicObstacle _pickForwardDistance(CCountW factor) {
        return super._pickForwardDistance(factor);
    }

    /********************************************************/


}
