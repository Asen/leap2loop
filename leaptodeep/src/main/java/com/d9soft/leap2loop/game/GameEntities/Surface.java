package com.d9soft.leap2loop.game.GameEntities;

import android.graphics.Canvas;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.Segment;
import com.d9soft.leap2loop.util.SafeList;

import java.util.Iterator;

public class Surface extends GameEntity {

    private SafeList<Segment> segments = new SafeList<>();

    public Surface()
    {
        Init();
    }

    public BasicObstacle getFirstObstacle()
    {
        for(Segment segment : segments) {
            BasicObstacle firstSegmentObstacle = segment.getFirstObstacle();
            if (firstSegmentObstacle != null)
                return firstSegmentObstacle;
        }
        return null;
    }

    public BasicObstacle getObstacleCharacterOn(Character character)
    {
        for(Segment segment : segments){
            BasicObstacle obstacle = segment.getObstacleCharacterOn(character);
            if(obstacle!=null) return obstacle;
        }
        return null;
    }

    public Segment getSegmentCharacterIn(Character character)
    {
        for(int i=0; i<segments.size(); ++i)
            if(segments.get(i).getRightBorder() > character.getLeftXPos())
                return segments.get(i);

        return null;
    }

    public void ReactOnLanding(Character character)
    {
        for(Segment segment : segments) {
            BasicObstacle obstacle = segment.getObstacleCharacterOn(character);
            if (obstacle != null)
                segment.ReactOnLanding(obstacle);
        }
    }

    public boolean isCharacterCrashed(Character character)
    {
        for(Segment segment : segments)
            if(segment.isCharacterCrashed(character)) return true;
        return false;
    }

    public void ReportCharacterMotion(Character character)
    {
        try{
            getSegmentCharacterIn(character).onCharacterMoved(character);
        }catch(NullPointerException e)
        {
            App.getInstance().Assert(false, "Character is not inside any segment!");
        }
    }

    private float _getLastSegmentBorder()
    {
        return segments.isEmpty() ? 0 : segments.getLast().getLeftBorder();
    }

    private synchronized void _tryAppendNewSegment()
    {
        final boolean isLastSegmentShown = _getLastSegmentBorder() < L2LConfig.GetSpaceWidth();

        if(!isLastSegmentShown)
            return;

        if(segments.isEmpty()) {
            /* If learning mode just started */
            if(Game.getLevel()== Game.ComplexityLevel.Level_Learning_MapPassive) {
                segments.add(new Segment(0, Game.getLevel()));
                segments.add(new Segment(segments.getLast().getRightBorder(), Game.ComplexityLevel.Level_Learning_MapActive));
            } else{
                segments.add(new Segment(0, L2LConfig.GetSpaceWidth() / 2));
            }
        }else{
            segments.add(new Segment(segments.getLast().getRightBorder(), Game.getLevel()));
        }
    }

    @Override
    public void Init() {
        super.Init();
    }

    @Override
    public void CommonMapMove() {

        if(segments.size()==0)
            return;

        segments.get(0).
                SetX(segments.get(0).getLeftBorder() - Game.GMap.GetTransition());
        for(int i=1; i<segments.size(); ++i)
            segments.get(i).SetX( segments.get(i-1).getRightBorder() );

        for(int i=0; i<segments.size(); ++i)
            segments.get(i).MoveInnerObjects();
    }


    @Override
    public void PrepareForDrawing()
    {
        // Clearing
        Iterator<Segment> bit = segments.iterator();
        while(bit.hasNext())
            if(bit.next().getRightBorder() <= 0)
                bit.remove();

        // Appending
        _tryAppendNewSegment();
    }

    @Override
    public void Draw(Canvas canvas) {

        for(Segment segment : segments)
            segment.Draw(canvas);

    }

}
