package com.d9soft.leap2loop.game.Obstacle.BasicObstacle;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.GameEntities.Character;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.util.CustomRandom;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPart;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;
import com.d9soft.leap2loop.util.RLoader;

import java.util.ArrayList;


public class PlatformObstacle extends BasicObstacle{

    protected static final float LANDING_PART = 1.0f; // of entire object
    protected static final Bitmap obstacleTexture = RLoader.Graphics.Obstacle.Platform.GetPlatform();

    protected float height = 0;
    protected Bitmap drawableObstacle = null;
    protected Bitmap drawableObstacleActive = null;


    protected int Value = 0;

    public PlatformObstacle(PlatformObstacle.Customization p_customization)
    {
        _Customize(p_customization);
    }

    private void _Customize(PlatformObstacle.Customization p_customization)
    {
        _pickHeight(p_customization.minHeight, p_customization.maxHeight);
        _pickValue(p_customization.minValue, p_customization.maxValue);
        _pickForwardDistance(p_customization.forwardDistance);
        _pickWidth(p_customization.minWidth, p_customization.maxWidth);

        freeSpace_startY = 0;
        freeSpace_endY = L2LConfig.GetSpaceHeight() - this.height;
        topOffset = (int) freeSpace_endY;

        App.getInstance().Log("New BottomObstacle created(width="+length+"; height="+height+"; value="+Value+")");
    }

    public int getValue()
    {
        return Value;
    }

    @Override
    protected ArrayList<RectF> getGeometryBuffer() {

        if(!geometryBuffer.isEmpty())
            return geometryBuffer;

        RectF upperPart = new RectF(leftOffset, topOffset,
                leftOffset+length, topOffset+height*0.15f);
        RectF downPart = new RectF(leftOffset+length*0.25f, upperPart.bottom,
                leftOffset+length*0.75f, upperPart.bottom+height*0.85f);

        geometryBuffer.add(upperPart);
        geometryBuffer.add(downPart);
        return geometryBuffer;
    }

    @Override
    public boolean isCharacterOnSurface(Character character) {

        float surfaceXL = leftOffset;
        float surfaceYL = topOffset - height*LANDING_PART;
        float surfaceXR = leftOffset + length;
        float surfaceYR = topOffset;
        RectF touchSurface = new RectF( surfaceXL, surfaceYL, surfaceXR, surfaceYR );

        return character.getTransition().isTouch(touchSurface);

    }

    @Override
    public void ReactOnLanding(PlatformObstacle obstacle) {
        return;
    }

    @Override
    public Bitmap getDrawable() {
        super.getDrawable();
        if(isActive){
            isActive=false;
            return drawableObstacle;
        }
        return drawableObstacle;
    }

    @Override
    protected void _createDrawable() {
        super._createDrawable();

        int width = (int)length;
        int height = (int) this.height;
        //int obstacleDrawableHeight = (int)(width*(1/WH_RATIO));

        //drawableObstacleActive = BitmapFactory.decodeResource( Game.gameResources, R.drawable.basic_pillar_active );
        //drawableObstacleActive = Bitmap.createScaledBitmap(drawableObstacleActive, width, height, true);

        drawableObstacle = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(drawableObstacle);
        Rect initialTextureRect=new Rect(0,0,obstacleTexture.getWidth(), obstacleTexture.getHeight());
        canvas.drawBitmap(
                obstacleTexture,
                initialTextureRect,
                new Rect(0,0,width,height),
                new Paint(Paint.FILTER_BITMAP_FLAG)
        );

        Paint paint = new Paint();
        paint.setColor(Color.argb(150, 0,0,0));
        paint.setTextSize(Math.min(length,height)/5f);
        paint.setFakeBoldText(true);
        String labelValue = Math.abs(Value)+"";
        String labelArrow = (Value > 0) ? "↓":"↑";
            if(Value==0) labelArrow = "";
        float Xv =  width/2f - paint.measureText(labelValue)/2f;
        float Yv = height/2 - paint.getTextSize();
        float Xa = width/2f - paint.measureText(labelArrow)/2f;
        float Ya =Yv + paint.getTextSize() + 5;
        canvas.drawText(labelValue, Xv, Yv, paint);
        canvas.drawText(labelArrow, Xa, Ya, paint);

    }


    /*
    **********************************************************
    ******************* Customization ************************
    **********************************************************
     */


    public static class Customization {

        ScrPartH minHeight = null,
                maxHeight = null;
        CCountW minWidth = null,
                maxWidth = null;
        CCountW forwardDistance = null;
        ScrPartH minValue = null,
                 maxValue = null;

        public Customization setHeight(ScrPartH p_minH, ScrPart p_maxH) {
            minHeight = new ScrPartH(p_minH);
            maxHeight = new ScrPartH(p_maxH);
            return this;
        }

        public Customization setWidth(CCountW p_minW, CCountW p_maxW) {
            minWidth = new CCountW(p_minW);
            maxWidth = new CCountW(p_maxW);
            return this;
        }

        public Customization setValue(ScrPartH p_minValue, ScrPartH p_maxValue) {
            minValue = new ScrPartH(p_minValue);
            maxValue = new ScrPartH(p_maxValue);
            return this;
        }

        public Customization setForwardDistance(CCountW p_fwdDistance) {
            forwardDistance = new CCountW(p_fwdDistance);
            return this;
        }
    }

    protected PlatformObstacle _pickHeight(ScrPartH p_minH, ScrPartH p_maxH)
    {
        if(p_minH==null || p_maxH==null)
            return this;

        int minHeight = (int) p_minH.Calc();
        int maxHeight = (int) p_maxH.Calc();

        if(minHeight > maxHeight)
            return this;

        height = CustomRandom.FromInterval(minHeight, maxHeight);
        freeSpace_startY = 0;
        freeSpace_endY = L2LConfig.GetSpaceHeight() - this.height;
        topOffset = (int) freeSpace_endY;

        _GeometryChanged();
        return this;
    }

    protected PlatformObstacle _pickWidth(CCountW p_minW, CCountW p_maxW)
    {
        int minWidth = (int) (p_minW==null ? new CCountW(1.5f).Calc() : p_minW.Calc());
        int maxWidth = (int) (p_maxW==null ? new CCountW(3f).Calc() : p_maxW.Calc());

        if(minWidth > maxWidth)
            return this;

        length = CustomRandom.FromInterval(minWidth, maxWidth);
        _GeometryChanged();

        return this;
    }

    protected PlatformObstacle _pickValue(ScrPartH p_minValue, ScrPartH p_maxValue)
    {
        if(p_minValue==null || p_maxValue==null)
            return this;

        int minValue = (int) p_minValue.Calc();
        int maxValue = (int) p_maxValue.Calc();

        if(minValue > maxValue)
            return this;

        Value = CustomRandom.FromInterval(minValue, maxValue);
        Value = (Value /5) * 5;
        //if(Value==0) Value = 5;

        _GeometryChanged();
        return this;
    }

    @Override
    protected BasicObstacle _pickForwardDistance(CCountW factor) {
        return super._pickForwardDistance(factor);
    }

    /********************************************************/

}
