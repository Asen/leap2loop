package com.d9soft.leap2loop.game.Obstacle.BasicObstacle.SegmentGenerator;


import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BasicObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.BottomObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.DoubleObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.PlatformZeroObstacle;
import com.d9soft.leap2loop.game.Obstacle.BasicObstacle.UpperObstacle;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountH;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;
import com.d9soft.leap2loop.util.Geometry.ScrPart.ScrPartH;

import java.util.Collection;

public class Segment_Learning {

    /* Single COMBINATION for learning mode */
    public static void V0(Collection<BasicObstacle> box)
    {
        float charactersCount = (L2LConfig.GetSpaceWidth()*5f) / L2LConfig.GetCharacterWidth();
        box.add(new BottomObstacle(
                new DoubleObstacle.Customization().
                        setBottomPartHeight(new ScrPartH(0.1f), new ScrPartH(0.25f)).
                        setWidth(new CCountW(1), new CCountW(1)).
                        setForwardDistance(new CCountW(charactersCount))
        ));
        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.17f), new ScrPartH(0.17f)).
                                setWidth(new CCountW(2.5f), new CCountW(2.5f)).
                                setForwardDistance(new CCountW(5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.3f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(1.5f), new CCountW(1.5f)).
                                setForwardDistance(new CCountW(4f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.35f), new ScrPartH(0.35f)).
                                setWidth(new CCountW(1.5f), new CCountW(1.5f)).
                                setForwardDistance(new CCountW(4f))
                )
        );

        box.add(new PlatformZeroObstacle(
                        new PlatformObstacle.Customization().
                                setHeight(new ScrPartH(0.5f), new ScrPartH(0.5f)).
                                setWidth(new CCountW(2f), new CCountW(2f)).
                                setForwardDistance(new CCountW(5f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.4f), new ScrPartH(0.4f)).
                                setWidth(new CCountW(1.5f), new CCountW(1.5f)).
                                setForwardDistance(new CCountW(2f))
                )
        );
        box.add(new DoubleObstacle(
                        new DoubleObstacle.Customization().
                                setFreeSpaceHeight(new CCountH(2.5f)).
                                setBottomPartHeight(new ScrPartH(0.3f), new ScrPartH(0.3f)).
                                setWidth(new CCountW(1.5f), new CCountW(1.5f)).
                                setForwardDistance(new CCountW(4f))
                )
        );

        box.add(new UpperObstacle(
                new DoubleObstacle.Customization().
                        setTopPartHeight(new ScrPartH(0.05f), new ScrPartH(0.05f)).
                        setWidth(new CCountW(1f), new CCountW(1f))
        ));

    }

}
