package com.d9soft.leap2loop.game;


import android.util.DisplayMetrics;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.game.GameEntities.Character;
import com.d9soft.leap2loop.util.Geometry.CCount.CCountW;


abstract public class L2LConfig {

    enum FPSValue{
        FPS_30, FPS_60, FPS_90,
        FPS_120, FPS_180, FPS_270,
        FPS_360, FPS_540
    }
    private static final FPSValue FPS = FPSValue.FPS_60;
    private static final int GAME_SECOND = 1080; //ms
    ////////////////////////////////////////////////////////////
    private static int SPACE_HEIGHT = 0;
    private static int SPACE_WIDTH = 0;
    ////////////////////////////////////////////////////////////
    private static int GROUND_LEVEL = 0;
    ////////////////////////////////////////////////////////////
    private static int CHARACTER_LEAP_HIGH = 0;
    private static int CHARACTER_HEIGHT = 0;
    ////////////////////////////////////////////////////////////
    private static CCountW DEFAULT_OBSTACLE_DISTANCE = null;
    ////////////////////////////////////////////////////////////

    public static void Set()
    {
        DisplayMetrics metrics = App.getInstance().getDisplayMetrics();

        SPACE_WIDTH = metrics.widthPixels;
        SPACE_HEIGHT = (int)(metrics.heightPixels * 0.9);
        //// Memory saving for large screens ////
        SPACE_HEIGHT = Math.min(SPACE_HEIGHT, 2048);
        /////////////////////////////////////////

        GROUND_LEVEL = SPACE_HEIGHT;
        // Non-smooth ground should be over obstacles` bottoms
        GROUND_LEVEL -= (metrics.heightPixels-SPACE_HEIGHT)*0.5f;

        CHARACTER_HEIGHT = SPACE_HEIGHT / 6;
        CHARACTER_LEAP_HIGH = (int)(CHARACTER_HEIGHT * 1.25);

        DEFAULT_OBSTACLE_DISTANCE = new CCountW(5);

        //App.getInstance().Log("[SpAW] "+GetCharacterWidth());
    }

    public static CCountW GetDefaultObstacleBetweenDistance()
    {
        return DEFAULT_OBSTACLE_DISTANCE;
    }

    public static float GetCharacterWidth()
    {
        return L2LConfig.CHARACTER_HEIGHT * Character.CHARACTER_WIDTH_FACTOR;
    }

    public static float GetCharacterXOffset()
    {
        return GetCharacterWidth();
    }

    public static float GetCharacterHeight()
    {
        return L2LConfig.CHARACTER_HEIGHT;
    }

    public static int GetCharacterLeapHigh()
    {
        return CHARACTER_LEAP_HIGH;
    }

    public static int GetSpaceHeight()
    {
        return SPACE_HEIGHT;
    }

    public static int GetSpaceWidth()
    {
        return SPACE_WIDTH;
    }

    public static int GetGroundLevel(){ return GROUND_LEVEL; }

    public static int GameOneSecond()
    {
        return GAME_SECOND;
    }

    public static int FPS()
    {
        switch(FPS)
        {
            case FPS_30: return 30;
            case FPS_60: return 60;
            case FPS_90: return 90;
            case FPS_120: return 120;
            case FPS_180: return 180;
            case FPS_270: return 270;
            case FPS_360: return 360;
            case FPS_540: return 540;
            default: return 30;
        }
    }

}
