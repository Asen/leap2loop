package com.d9soft.leap2loop;

import android.app.Application;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.d9soft.leap2loop.util.Audio;

public class App extends Application {

    private static String LOG_TAG = "[L2L_LOG]";
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        InitAppRun();
    }

    private void InitAppRun()
    {
        app = this;
        InitStatics();
    }

    private void InitStatics()
    {
        Audio.InitSounds();
        Log("Statics is initialized.");
    }

    public static App getInstance()
    {
        return app;
    }

    public DisplayMetrics getDisplayMetrics() {
        DisplayMetrics metrics = new DisplayMetrics();
        Display d = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        d.getMetrics(metrics);
        return metrics;
    }

    /*public int getResIdentifier(String name, String type)
    {
        return getResources().getIdentifier(name, type, getPackageName());
    }

    public Bitmap decDrawableRes(int resource_id)
    {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        opts.inDither = false;
        opts.inPreferredConfig = Bitmap.Config.RGB_565;
        opts.inSampleSize = 1;

        return BitmapFactory.decodeResource(getResources(), resource_id, opts);
    }*/

    public String getStringRes(int resource_id)
    {
        return getResources().getString(resource_id);
    }

    public String[] getArrayRes(int resource_id)
    {
        return getResources().getStringArray(resource_id);
    }

    public void Vibrate(long vibrateDuration)
    {
        Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(vibrateDuration);
    }

    public void Log(String message)
    {
        if (message == null || message.length() == 0)
            return;

        if(BuildConfig.DEBUG) {
            String callingInfo = _GetCallingElementInfo();
            Log.i(LOG_TAG, "[" + callingInfo + "]\n" + message);
        }else{
            Log.i(LOG_TAG,  message);
        }
    }

    public void Assert(boolean condition, String message)
    {
        if(!condition)
        {
            String callingInfo = _GetCallingElementInfo();
            if(message!=null && message.length()>0)
                Log.e(LOG_TAG, "["+callingInfo+"]\n"+message);

            if(BuildConfig.DEBUG)
                throw new RuntimeException();
        }
    }

    private String _GetCallingElementInfo()
    {
        try{
            StackTraceElement[] stack = Thread.currentThread().getStackTrace();
            String _class = stack[4].getClassName();
            String _method = stack[4].getMethodName();
            return _class+"."+_method;
        }catch(Exception e)
        {
            return "<no info>";
        }
    }

}
