package com.d9soft.leap2loop;

import com.google.android.vending.licensing.LicenseCheckerCallback;

public class L2License implements LicenseCheckerCallback {

    public static final int LICENSED = 0x0;
    public static final int NOT_LICENSED = 0x1;
    public static final int LICENSED_OLD_KEY = 0x2;
    public static final int ERROR_NOT_MARKET_MANAGED = 0x3;
    public static final int ERROR_SERVER_FAILURE = 0x4;
    public static final int ERROR_OVER_QUOTA = 0x5;
    public static final int ERROR_CONTACTING_SERVER = 0x101;
    public static final int ERROR_INVALID_PACKAGE_NAME = 0x102;
    public static final int ERROR_NON_MATCHING_UID = 0x103;

    private Main callingActivity = null;

    private L2License(){}

    public L2License(Main mainActivity)
    {
        callingActivity = mainActivity;
    }

    @Override
    public void allow(int reason) {
        App.getInstance().Log("[LICENCE] allow");
        callingActivity.ReportLicenseCheckSucceed();
    }

    @Override
    public void dontAllow(int reason) {
        App.getInstance().Log("[LICENCE] disallow");
        callingActivity.ReportLicenseCheckFailed(reason);
    }

    @Override
    public void applicationError(int errorCode) {
        App.getInstance().Log("[LICENCE] error code: "+errorCode);
        dontAllow(errorCode);
    }
}