package com.d9soft.leap2loop;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.d9soft.leap2loop.game.Game;
import com.d9soft.leap2loop.game.L2LConfig;
import com.d9soft.leap2loop.util.Audio;
import com.d9soft.leap2loop.util.GRecords.ActionResolver;
import com.d9soft.leap2loop.util.L2LData;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.example.games.basegameutils.GameHelper;

import java.util.HashMap;


public class GameActivity extends Activity
{

    /*private final int MENU_ITEM_LEARNING = Menu.FIRST+1;
    private final int MENU_ITEM_PAUSE_RESUME = Menu.FIRST+2;*/
    private AlertDialog resumeDialog = null;
    private Game game = null;
    private L2LGameHelper GPGS = null;
    private TextView
            distanceBoard = null,
            scoreBoard = null;
    private ImageButton stateButton = null;
    private Button backButton = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        SetActionBar();
        App.getInstance().Log("Activity is CREATED");

        L2LConfig.Set();
        game = (Game)findViewById(R.id.gameView);
        game.Run(this);

        GPGS = new L2LGameHelper();
        GPGS.Init();
    }

    @Override
    public void onBackPressed() {
        ShowExitDialog();
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.getInstance().Log("Activity is PAUSED");

        if(game!=null)
            game.Pause();
    }

    @Override
    protected void onStart() {
        super.onStart();

        GPGS.gameHelper.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.getInstance().Log("Activity is STOPPED");

        GPGS.gameHelper.onStop();

        if(game!=null)
            game.Pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.getInstance().Log("Activity is RESUMED");

        invalidateOptionsMenu();
        if(game!=null && game.isGamePaused() && !game.isGameLost())
            ShowResumeDialog();

        if(!GPGS.gameHelper.isConnecting())
            game.Pause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getInstance().Log("Activity is DESTROYED");
    }

    @Override
    public void invalidateOptionsMenu() {
        super.invalidateOptionsMenu();

        if(stateButton!=null && game!=null)
        {
            int img = game.isGamePaused() ? R.drawable.actionbar_btn_play: R.drawable.actionbar_btn_pause;
            stateButton.setImageResource(img);
        }

        if(backButton!=null)
        {
            boolean learningMode = L2LData.getLearningModeState();
            backButton.setText(learningMode ? R.string.learning_mode_back : R.string.game_activity_back);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        GPGS.gameHelper.onActivityResult(requestCode, resultCode, data);
    }

    private void ShowResumeDialog()
    {
        ContextThemeWrapper themedContext = new ContextThemeWrapper(this, R.style.AppStandardDialog);
        AlertDialog.Builder resumeAlert = new AlertDialog.Builder(themedContext);
        resumeAlert.setCancelable(false);
        resumeAlert.setMessage(R.string.game_activity_continue_dialog_text);
        resumeAlert.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (game != null)
                    game.Resume();
                invalidateOptionsMenu();
            }
        });
        resumeAlert.setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        if(resumeDialog==null || !resumeDialog.isShowing())
            resumeDialog = resumeAlert.show();
    }

    private void ShowExitDialog()
    {
        if(game==null)
            return;
        game.Pause();
        /////////////////////////////////////////////////////////
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setTitle(R.string.game_activity_exit_dialog_title);
        alert.setMessage(R.string.game_activity_exit_dialog_text);
        alert.setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Main.StartActivity();
                if (game != null)
                    game.ReportExit();
                game = null;
                finish();
                System.gc();
            }
        });
        alert.setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (game != null)
                    game.Resume();
                invalidateOptionsMenu();
                dialog.cancel();
            }
        });
        alert.show();
    }

    private void SetActionBar()
    {
        if(getActionBar()==null)
            return;

        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayShowCustomEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setCustomView(R.layout.game_action_bar);

        /*Drawable drawable = ContextCompat.getDrawable(this, R.drawable.actionbar_bg);
        getActionBar().setBackgroundDrawable(drawable);*/
        invalidateOptionsMenu();

        distanceBoard = (TextView)getActionBar().getCustomView().findViewById(R.id.distance_board);
        scoreBoard = (TextView)getActionBar().getCustomView().findViewById(R.id.score_board);
        stateButton = (ImageButton)getActionBar().getCustomView().findViewById(R.id.state_btn);
        backButton = (Button)getActionBar().getCustomView().findViewById(R.id.back_btn);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Audio.PlayClick();
                GameActivity.this.onBackPressed();
            }
        });

        stateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Audio.PlayClick();
                if(game==null)
                    return;
                if (game.isGamePaused()) {
                    game.Resume();
                    ((ImageButton) v).setImageResource(R.drawable.actionbar_btn_pause);
                } else {
                    game.Pause();
                    ((ImageButton) v).setImageResource(R.drawable.actionbar_btn_play);
                }
            }
        });
    }

    int lastDistance=-1, lastScore=-1;
    public void UpdateGameInfo(int distance, int score)
    {
        if(distanceBoard!=null && distance!= lastDistance) {
            distanceBoard.setText(distance + "m");
            lastDistance = distance;
        }

        if(scoreBoard!=null && lastScore!=score) {
            scoreBoard.setText(score + "");
            lastScore = score;
        }
    }

    public void UpdateAfterGameStart()
    {
        this.invalidateOptionsMenu();
    }

    public L2LGameHelper getGPGSInterface()
    {
        return GPGS;
    }



    public class L2LGameHelper implements
            ActionResolver, GameHelper.GameHelperListener
    {
        String BOARD_POINTS = null;
        String BOARD_DISTANCE = null;

        String ACHIEVEMENT_BRONZE = null;
        String ACHIEVEMENT_SILVER = null;
        String ACHIEVEMENT_GOLD = null;
        String ACHIEVEMENT_BRONZE_DIST = null;
        String ACHIEVEMENT_SILVER_DIST = null;
        String ACHIEVEMENT_GOLD_DIST = null;

        private GameHelper gameHelper = null;
        private String postponedScore_Board = null;
        private int postponedScore_Score = 0;

        private HashMap<String, Integer> scoreTable = new HashMap();

        private void Init()
        {
            //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            gameHelper = new GameHelper(GameActivity.this, GameHelper.CLIENT_GAMES);
            gameHelper.setMaxAutoSignInAttempts(3);
            gameHelper.setConnectOnStart(true);
            gameHelper.enableDebugLog(true);
            gameHelper.setup(this);

            BOARD_POINTS = getResources().getString(R.string.leaderboard_points);
            BOARD_DISTANCE = getResources().getString(R.string.leaderboard_distance);

            ACHIEVEMENT_BRONZE = getResources().getString(R.string.achievement_bronze);
            ACHIEVEMENT_SILVER = getResources().getString(R.string.achievement_silver);
            ACHIEVEMENT_GOLD = getResources().getString(R.string.achievement_gold);
            ACHIEVEMENT_BRONZE_DIST = getResources().getString(R.string.achievement_bronze_distance);
            ACHIEVEMENT_SILVER_DIST = getResources().getString(R.string.achievement_silver_distance);
            ACHIEVEMENT_GOLD_DIST = getResources().getString(R.string.achievement_golden_distance);
        }

        public void UnlockAchievements()
        {
            safeUnlockAchievementIfExists();
        }

        public void PostBestScore_Points()
        {
            safeScorePosting(BOARD_POINTS, L2LData.getSavedScore());
        }

        public void PostBestScore_Distance()
        {
            safeScorePosting(BOARD_DISTANCE, L2LData.getSavedDistance());
        }

        public void UpdateScoreTables()
        {
            if(!getSignedInGPGS())
            {
                loginGPGS();
                return;
            }

            Update_UserBest(BOARD_POINTS, "U_BEST_POINTS");
            Update_UserBest(BOARD_DISTANCE, "U_BEST_DISTANCE");
            Update_Bests(BOARD_POINTS, "WEEKLY_BEST_POINTS", LeaderboardVariant.TIME_SPAN_WEEKLY);
            Update_Bests(BOARD_DISTANCE, "WEEKLY_BEST_DISTANCE", LeaderboardVariant.TIME_SPAN_WEEKLY);
            Update_Bests(BOARD_POINTS, "EVER_BEST_POINTS", LeaderboardVariant.TIME_SPAN_ALL_TIME);
            Update_Bests(BOARD_DISTANCE, "EVER_BEST_DISTANCE", LeaderboardVariant.TIME_SPAN_ALL_TIME);
        }

        public void UpdateScoreTable(int score, int distance)
        {
            for(String key : new String[]{"U_BEST_POINTS", "WEEKLY_BEST_POINTS", "EVER_BEST_POINTS"})
                if(scoreTable.containsKey(key) && score > scoreTable.get(key))
                    scoreTable.put(key, score);

            for(String key : new String[]{"U_BEST_DISTANCE", "WEEKLY_BEST_DISTANCE", "EVER_BEST_DISTANCE"})
                if(scoreTable.containsKey(key) && distance > scoreTable.get(key))
                    scoreTable.put(key, distance);

            UpdateScoreTables();
        }

        public Integer GetBestUserPoints()
        {
            return scoreTable.containsKey("U_BEST_POINTS") ?
                    scoreTable.get("U_BEST_POINTS") :
                    L2LData.getSavedScore();
        }

        public Integer GetBestUserDistance()
        {
            return scoreTable.containsKey("U_BEST_DISTANCE") ?
                    scoreTable.get("U_BEST_DISTANCE") :
                    L2LData.getSavedDistance();
        }

        public Integer GetWeeklyPoints()
        {
            return scoreTable.containsKey("WEEKLY_BEST_POINTS") ?
                    scoreTable.get("WEEKLY_BEST_POINTS") :
                    0;
        }

        public Integer GetWeeklyDistance()
        {
            return scoreTable.containsKey("WEEKLY_BEST_DISTANCE") ?
                    scoreTable.get("WEEKLY_BEST_DISTANCE") :
                    0;
        }

        public Integer GetBestPoints()
        {
            return scoreTable.containsKey("EVER_BEST_POINTS") ?
                    scoreTable.get("EVER_BEST_POINTS") :
                    0;
        }

        public Integer GetBestDistance()
        {
            return scoreTable.containsKey("EVER_BEST_DISTANCE") ?
                    scoreTable.get("EVER_BEST_DISTANCE") :
                    0;
        }

        /* Synchronize local storage with global storage */
        private void Update_Local_UserBest(final String BOARD, final int value)
        {
            if(BOARD.compareToIgnoreCase(BOARD_POINTS)==0)
                L2LData.trySaveNewScore(value);

            if(BOARD.compareToIgnoreCase(BOARD_DISTANCE)==0)
                L2LData.trySaveNewDistance(value);
        }

        private void Update_UserBest(final String BOARD, final String KEY)
        {
            Games.Leaderboards.loadCurrentPlayerLeaderboardScore(
                    gameHelper.getApiClient(),
                    BOARD,
                    LeaderboardVariant.TIME_SPAN_ALL_TIME,LeaderboardVariant.COLLECTION_PUBLIC
            ).setResultCallback(new ResultCallbacks<Leaderboards.LoadPlayerScoreResult>() {
                @Override
                public void onSuccess(Leaderboards.LoadPlayerScoreResult loadPlayerScoreResult) {
                    String score = loadPlayerScoreResult.getScore().getDisplayScore();
                    int scoreValue = score==null ? 0 : Integer.parseInt(score);
                    scoreTable.put(KEY, scoreValue);

                    // Synchronization
                    Update_Local_UserBest(BOARD, scoreValue);
                }

                @Override
                public void onFailure(Status status) {
                    App.getInstance().Log("[GPGS Fail]" + status.getStatusMessage());
                }
            });
        }

        private void Update_Bests(final String BOARD, final String KEY, final int PERIOD)
        {
            Games.Leaderboards.loadTopScores(
                    gameHelper.getApiClient(),
                    BOARD,
                    PERIOD, LeaderboardVariant.COLLECTION_PUBLIC,
                    1, false
            ).setResultCallback(new ResultCallbacks<Leaderboards.LoadScoresResult>() {
                @Override
                public void onSuccess(Leaderboards.LoadScoresResult loadScoresResult) {
                    String score = loadScoresResult.getScores().getCount()>0 ?
                            loadScoresResult.getScores().get(0).getDisplayScore():
                            "0";
                    scoreTable.put(KEY, score==null ? 0 : Integer.parseInt(score));
                }

                @Override
                public void onFailure(Status status) {
                    App.getInstance().Log("[GPGS Fail]" + status.getStatusMessage());
                }
            });
        }


        private void safeScorePosting(String board, int score)
        {
            // If score must be sent, but user isn`t signed in
            if(!getSignedInGPGS()) {
                postponedScore_Board = board;
                postponedScore_Score = score;
                loginGPGS();
                return;
            }

            Games.Leaderboards.submitScore(gameHelper.getApiClient(), board, score);
        }

        private void safeUnlockAchievementIfExists()
        {
            // If score must be sent, but user isn`t signed in
            if(!getSignedInGPGS()) {
                loginGPGS();
                return;
            }

            int bestKnownScore = L2LData.getSavedScore();
            int bestKnownDist = L2LData.getSavedDistance();

            /* Point achievements */
            if(bestKnownScore>=30)
                Games.Achievements.unlock(gameHelper.getApiClient(), ACHIEVEMENT_BRONZE);
            if(bestKnownScore>=200)
                Games.Achievements.unlock(gameHelper.getApiClient(), ACHIEVEMENT_SILVER);
            if(bestKnownScore>=300)
                Games.Achievements.unlock(gameHelper.getApiClient(), ACHIEVEMENT_GOLD);

            /* Distance achievements */
            if(bestKnownDist>=1000)
                Games.Achievements.unlock(gameHelper.getApiClient(), ACHIEVEMENT_BRONZE_DIST);
            if(bestKnownDist>=1500)
                Games.Achievements.unlock(gameHelper.getApiClient(), ACHIEVEMENT_SILVER_DIST);
            if(bestKnownDist>=2000)
                Games.Achievements.unlock(gameHelper.getApiClient(), ACHIEVEMENT_GOLD_DIST);

            App.getInstance().Log("[ACHIEVEMENT] "+bestKnownDist+" - "+bestKnownScore);
        }

        @Override
        public boolean getSignedInGPGS() {
            return gameHelper==null ? false : gameHelper.isSignedIn();
        }

        @Override
        public void loginGPGS() {
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        gameHelper.beginUserInitiatedSignIn();
                    }
                });
            } catch (Exception e) {
                App.getInstance().Assert(false, "Login GPGS failed.");
            }
        }

        @Override
        public void submitScoreGPGS(int score) {
            return;
        }

        @Override
        public void unlockAchievementGPGS(String achievementId) {
            Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
        }

        @Override
        public void getLeaderboardGPGS() {
            if(getSignedInGPGS())
                startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(gameHelper.getApiClient()), 100);
        }

        @Override
        public void getAchievementsGPGS() {
            startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 101);
        }



        @Override
        public void onSignInFailed() {
            //App.getInstance().Assert(false, "GPGS sign in failed!");
        }

        @Override
        public void onSignInSucceeded() {

            UpdateScoreTables();
            safeUnlockAchievementIfExists();

            // If score was postponed
            if(postponedScore_Board !=null)
            {
                safeScorePosting(postponedScore_Board, postponedScore_Score);
                postponedScore_Board = null;
                postponedScore_Score = 0;
            }

            App.getInstance().Log("[GPGS] Succeed");
        }
    }

}
