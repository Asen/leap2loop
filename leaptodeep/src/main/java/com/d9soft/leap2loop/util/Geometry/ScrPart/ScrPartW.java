package com.d9soft.leap2loop.util.Geometry.ScrPart;


import com.d9soft.leap2loop.game.L2LConfig;

public class ScrPartW extends ScrPart {

    public ScrPartW(float p_partOfScreen) {
        super(p_partOfScreen);
    }

    public ScrPartW(ScrPart copy) {
        super(copy);
    }

    @Override
    public float Calc() {
        return partOfScreen * L2LConfig.GetSpaceWidth();
    }
}
