package com.d9soft.leap2loop.util.Geometry.ScrPart;


import com.d9soft.leap2loop.game.L2LConfig;

public class ScrPartH extends ScrPart {

    public ScrPartH(float p_partOfScreen) {
        super(p_partOfScreen);
    }

    public ScrPartH(ScrPart copy) {
        super(copy);
    }

    @Override
    public float Calc() {
        return partOfScreen * L2LConfig.GetSpaceHeight();
    }
}
