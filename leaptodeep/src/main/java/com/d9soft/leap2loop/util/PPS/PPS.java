package com.d9soft.leap2loop.util.PPS;

import com.d9soft.leap2loop.game.L2LConfig;

/**
 *
 * FPS Helper:
 * Pixels per Second
 *
 */

public class PPS {

    public float pxPerSecond = 0f;

    public PPS(float p_pixelsPerSecond)
    {
        pxPerSecond = p_pixelsPerSecond;
    }

    public PPS(PPS copy)
    {
        if(copy!=null)
            this.pxPerSecond = copy.pxPerSecond;
    }

    public float CalcPerFrame()
    {
        float perFrame = pxPerSecond / L2LConfig.FPS();
        return perFrame;
    }

    public static float CalcPerFrame(float p_pxPerSecond)
    {
        float perFrame = p_pxPerSecond / L2LConfig.FPS();
        return perFrame;
    }

    /*public float CalcPerFrame(int p_FPS)
    {
        return pxPerSecond/p_FPS;
    }*/

}
