package com.d9soft.leap2loop.util.Geometry.CCount;

/*
*
* This class represents the geometric
* size of L2L Character`s height.
*
* */

import com.d9soft.leap2loop.game.L2LConfig;

public class CCountH extends CCount {

    public CCountH(float p_charactersCount) {
        super(p_charactersCount);
    }

    public CCountH(CCount copy) {
        super(copy);
    }

    @Override
    public float Calc() {
        return charactersCount * L2LConfig.GetCharacterHeight();
    }
}
