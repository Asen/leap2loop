package com.d9soft.leap2loop.util;

public class Texture {

    public float X = 0;
    public float Y = 0;
    public int Num = 0;

    public Texture(float _x, float _y)
    {
        X = _x;
        Y = _y;
    }

    public Texture(double _x, double _y)
    {
        X = (float)_x;
        Y = (float)_y;
    }

    public Texture setNum(int N)
    {
        Num = N;
        return this;
    }

}
