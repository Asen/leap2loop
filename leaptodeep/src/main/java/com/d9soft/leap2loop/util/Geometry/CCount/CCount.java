package com.d9soft.leap2loop.util.Geometry.CCount;

/*
*
* This class represents the geometric
* size of L2L Character`s abstract dimension.
*
* */

public abstract class CCount {

    public float charactersCount;

    public CCount(float p_charactersCount){
        charactersCount = p_charactersCount;
    }
    public CCount(CCount copy)
    {
        if(copy!=null)
            this.charactersCount = copy.charactersCount;
    }
    public float Value()
    {
        return  charactersCount;
    }

    public abstract float Calc();

}
