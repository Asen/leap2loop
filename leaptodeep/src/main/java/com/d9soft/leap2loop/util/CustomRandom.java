package com.d9soft.leap2loop.util;

import com.d9soft.leap2loop.App;

import java.util.Random;

public class CustomRandom {

    //private static Random Generator = new Random();

    public static int FromInterval(int min, int max)
    {
        App.getInstance().Assert(min<=max, "FromInterval::min="+min+"; max="+max);

        int absMax = Math.max(Math.abs(min), Math.abs(max));
        min+=absMax;
        max+=absMax;
        int number = new Random().nextInt(max-min+1)+min;

        return number - absMax;
    }

}
