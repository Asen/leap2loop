package com.d9soft.leap2loop.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.d9soft.leap2loop.App;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class RLoader {

    public static class Data
    {
        public static class Learning
        {
            private static String[] learningInfo = null;

            public static String[] GetMotionMap()
            {
                String fName = "GAME/other/learning.txt";

                if(learningInfo!=null)
                    return learningInfo;

                try{
                    InputStream file = App.getInstance().getAssets().open(fName);
                    Reader reader = new InputStreamReader(file, "UTF-8");
                    BufferedReader bufferedReader = new BufferedReader(reader);

                    StringBuffer buffer = new StringBuffer();
                    String line = null;

                    while((line = bufferedReader.readLine())!=null)
                        buffer.append(line);

                    learningInfo = buffer.toString().split(";");
                    return learningInfo;
                }catch (Exception e)
                {
                    App.getInstance().Assert(false, "LearningInfo fail!");
                    return new String[0];
                }
            }

        }
    }

    public static class Graphics
    {

        private static BitmapFactory.Options GetOptions()
        {
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;
            opts.inDither = false;
            opts.inPreferredConfig = Bitmap.Config.RGB_565;
            opts.inSampleSize = 2;
            opts.inMutable = true;
            return opts;
        }

        public static class Common
        {
            private static Bitmap pause = null;
            private static Bitmap finger = null;

            public static Bitmap GetGamePause()
            {
                String fName = "GAME/other/game_pause.png";

                try{
                    if(pause==null) {
                        InputStream file = App.getInstance().getAssets().open(fName);
                        pause = BitmapFactory.decodeStream(file, null, GetOptions());
                    }
                    return pause;
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }
            }

            public static Bitmap GetGameFinger()
            {
                String fName = "GAME/other/push.png";

                try{
                    if(finger==null) {
                        InputStream file = App.getInstance().getAssets().open(fName);
                        finger = BitmapFactory.decodeStream(file, null, GetOptions());
                    }
                    return finger;
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }
            }

        }

        public static class Background
        {
            public static Bitmap GetParticle(int number)
            {
                String bgFolder = "GAME/background/bg_%d.png";
                String fName = String.format(bgFolder,number);

                try{
                    InputStream file = App.getInstance().getAssets().open(fName);
                    return BitmapFactory.decodeStream(file, null, GetOptions());
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }

           /*int id = App.getInstance().getResIdentifier("bg_" + number, "drawable");
           return App.getInstance().decDrawableRes(id);*/
            }

        }


        public static class Character
        {
            public static Bitmap GetDefault()
            {
                String fName = "GAME/character/default.png";

                try{
                    InputStream file = App.getInstance().getAssets().open(fName);
                    return BitmapFactory.decodeStream(file, null, GetOptions());
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }

                //return App.getInstance().decDrawableRes(R.drawable.char_default);

            }

            public static Bitmap GetShrunk()
            {
                String fName = "GAME/character/shrunk.png";

                try{
                    InputStream file = App.getInstance().getAssets().open(fName);
                    return BitmapFactory.decodeStream(file, null, GetOptions());
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }

                //return App.getInstance().decDrawableRes(R.drawable.char_shrunk);

            }

        }


        public static class Ground
        {
            public static Bitmap GetGround()
            {
                String fName = "GAME/ground/ground.png";

                try{
                    InputStream file = App.getInstance().getAssets().open(fName);
                    return BitmapFactory.decodeStream(file, null, GetOptions());
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }

                //return App.getInstance().decDrawableRes(R.drawable.ground);

            }

            public static Bitmap GetLava()
            {
                String fName = "GAME/ground/lava.png";

                try{
                    InputStream file = App.getInstance().getAssets().open(fName);
                    return BitmapFactory.decodeStream(file, null, GetOptions());
                }catch (Exception e)
                {
                    App.getInstance().Log("Loading error!");
                    return Bitmap.createBitmap(1,1, Bitmap.Config.ALPHA_8);
                }

                //return App.getInstance().decDrawableRes(R.drawable.ground_lava);

            }

        }


        public static class Obstacle {

            public static class Platform {

                private static Bitmap platform = null;
                private static Bitmap static_platform = null;

                public static Bitmap GetPlatform() {
                    String fName = "GAME/obstacles/platform.png";

                    if (platform != null)
                        return platform;

                    try {
                        InputStream file = App.getInstance().getAssets().open(fName);
                        platform = BitmapFactory.decodeStream(file, null, GetOptions());
                        return platform;
                    } catch (Exception e) {
                        App.getInstance().Log("Loading error!");
                        return Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
                    }

                    //return App.getInstance().decDrawableRes(R.drawable.obstacle_platform);

                }

                public static Bitmap GetStaticPlatform() {
                    String fName = "GAME/obstacles/static_platform.png";

                    if (static_platform != null)
                        return static_platform;

                    try {
                        InputStream file = App.getInstance().getAssets().open(fName);
                        static_platform = BitmapFactory.decodeStream(file, null, GetOptions());
                        return static_platform;
                    } catch (Exception e) {
                        App.getInstance().Log("Loading error!");
                        return Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
                    }

                    //return App.getInstance().decDrawableRes(R.drawable.obstacle_platform);

                }

            }

            public static class Double {
                private static Bitmap top = null;
                private static Bitmap bottom = null;

                public static Bitmap GetTop() {
                    String fName = "GAME/obstacles/double_top.png";

                    if (top != null)
                        return top;

                    try {
                        InputStream file = App.getInstance().getAssets().open(fName);
                        top = BitmapFactory.decodeStream(file, null, GetOptions());
                        return top;
                    } catch (Exception e) {
                        App.getInstance().Log("Loading error!");
                        return Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
                    }

                    //return App.getInstance().decDrawableRes(R.drawable.obstacle_double_top);

                }

                public static Bitmap GetBottom() {
                    String fName = "GAME/obstacles/double_bottom.png";

                    if (bottom != null)
                        return bottom;

                    try {
                        InputStream file = App.getInstance().getAssets().open(fName);
                        bottom = BitmapFactory.decodeStream(file, null, GetOptions());
                        return bottom;
                    } catch (Exception e) {
                        App.getInstance().Log("Loading error!");
                        return Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
                    }

                    //return App.getInstance().decDrawableRes(R.drawable.obstacle_double_bottom);

                }

            }
        }

    }

}
