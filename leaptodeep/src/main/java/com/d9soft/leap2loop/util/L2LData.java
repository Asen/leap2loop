package com.d9soft.leap2loop.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import com.d9soft.leap2loop.App;
import com.securepreferences.SecurePreferences;
import com.tozny.crypto.android.AesCbcWithIntegrity;

import java.security.GeneralSecurityException;

public class L2LData {

    private static final String SHARED_PREF_NAME = "L2L";
    private static final String SHARED_PREF_SCORE_KEY = "Score";
    private static final String SHARED_PREF_DISTANCE_KEY = "Distance";
    private static final String SHARED_PREF_PLAYED_GAMES_AMOUNT = "PlayedGamesCount";
    private static final String SHARED_PREF_LEARN_MODE = "LearningMode";
    private static final String SHARED_PREF_SOUND = "Sound";
    private static final String SHARED_PREF_MUSIC = "Music";
    private static SecurePreferences PREFS = null;

    private static SharedPreferences getDefaultPreferences()
    {
        Context context = App.getInstance();
        App.getInstance().Assert(context != null, "SharedPreferences exception: app context not found!");

        if(PREFS == null)
            try {
                String pwd = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                AesCbcWithIntegrity.SecretKeys secKeys;
                secKeys = AesCbcWithIntegrity.generateKeyFromPassword(pwd, pwd.getBytes(), 256);
                PREFS = new SecurePreferences(context, secKeys, SHARED_PREF_NAME);
            } catch (GeneralSecurityException e) {
                App.getInstance().Assert(false, "AES keyGen error: "+e.getMessage());
            }

        return PREFS;
    }

    public static void trySaveNewScore(final int newScore)
    {
        SharedPreferences data = getDefaultPreferences();
        Integer savedScore = getSavedScore();

        if( savedScore < newScore )
            data.edit().putInt(SHARED_PREF_SCORE_KEY, newScore).apply();
    }

    public static int getSavedScore()
    {
        SharedPreferences data = getDefaultPreferences();
        Integer savedScore;
            try{
                savedScore = data.getInt(SHARED_PREF_SCORE_KEY, 0);
            }catch (ClassCastException e){
                savedScore = 0;
            }
        return savedScore;
    }

    public static void trySaveNewDistance(final int newDistance)
    {
        SharedPreferences data = getDefaultPreferences();
        Integer savedDistance = getSavedDistance();

        if( savedDistance < newDistance )
            data.edit().putInt(SHARED_PREF_DISTANCE_KEY, newDistance).apply();
    }

    public static int getSavedDistance()
    {
        SharedPreferences data = getDefaultPreferences();
        Integer savedDistance;
            try{
                savedDistance = data.getInt(SHARED_PREF_DISTANCE_KEY, 0);
            }catch (ClassCastException e){
                savedDistance = 0;
            }
        return savedDistance;
    }

    public static void updatePlayedGamesAmount()
    {
        SharedPreferences data = getDefaultPreferences();
        int newPlayedGamesAmount = getPlayedGamesAmount()+1;
        data.edit().putInt(SHARED_PREF_PLAYED_GAMES_AMOUNT, newPlayedGamesAmount).apply();
    }

    public static int getPlayedGamesAmount()
    {
        SharedPreferences data = getDefaultPreferences();
        Integer playedGames;
            try{
                playedGames = data.getInt(SHARED_PREF_PLAYED_GAMES_AMOUNT, 0);
            }catch (ClassCastException e){
                playedGames = 0;
            }
        return playedGames;
    }

    public static void setLearningModeState(final boolean state)
    {
        SharedPreferences data = getDefaultPreferences();
        data.edit().putBoolean(SHARED_PREF_LEARN_MODE, state).apply();
    }

    public static boolean getLearningModeState()
    {
        SharedPreferences data = getDefaultPreferences();
        Boolean isLearningMode;
            try{
                isLearningMode = data.getBoolean(SHARED_PREF_LEARN_MODE, false);
            }catch (ClassCastException e){
                isLearningMode = false;
            }
        return isLearningMode;
    }

    public static void setSoundState(final boolean state)
    {
        SharedPreferences data = getDefaultPreferences();
        data.edit().putBoolean(SHARED_PREF_SOUND, state).apply();
    }

    public static boolean getSoundState()
    {
        SharedPreferences data = getDefaultPreferences();
        Boolean isSoundOn;
        try{
            isSoundOn = data.getBoolean(SHARED_PREF_SOUND, true);
        }catch (ClassCastException e){
            isSoundOn = true;
        }
        return isSoundOn;
    }

    public static void setMusicState(final boolean state)
    {
        SharedPreferences data = getDefaultPreferences();
        data.edit().putBoolean(SHARED_PREF_MUSIC, state).apply();
    }

    public static boolean getMusicState()
    {
        SharedPreferences data = getDefaultPreferences();
        Boolean isMusicOn;
        try{
            isMusicOn = data.getBoolean(SHARED_PREF_MUSIC, true);
        }catch (ClassCastException e){
            isMusicOn = true;
        }
        return isMusicOn;
    }

}
