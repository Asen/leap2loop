package com.d9soft.leap2loop.util.Geometry;

import android.graphics.RectF;

public class RectTransitionVector {

    private RectF start;
    private RectF end;
    private final RectF wRect = new RectF();

    public RectTransitionVector()
    {
        start = new RectF();
        end = new RectF();
    }

    public void Init(RectF l_start, RectF l_end)
    {
        start.left = l_start.left;
        start.top = l_start.top;
        start.bottom = l_start.bottom;
        start.right = l_start.right;
        //////////////////////////////
        end.left = l_end.left;
        end.top = l_end.top;
        end.bottom = l_end.bottom;
        end.right = l_end.right;
    }

    public float GetXTransition()
    {
        return end.left - start.left;
    }

    public float GetYTransition()
    {
        return end.top - start.top;
    }

    public boolean isIntersected(RectF rectStatic)
    {
        if(Math.abs(GetXTransition()) > Math.abs(GetYTransition())) {

            int startX = (int) Math.floor(Math.min(start.left, end.left));
            int endX = (int) Math.ceil(Math.max(start.left, end.left));

            for (int x = startX; x <= endX ; x++) {
                float y = _getYByX(x);
                wRect.set(x, y, x + end.width(), y + end.height());
                if(rectStatic.intersect(wRect) || wRect.intersect(rectStatic))
                    return true;
            }
        }
        else {
            int startY = (int) Math.floor(Math.min(start.top, end.top));
            int endY = (int) Math.ceil(Math.max(start.top, end.top));

            for (int y = startY; y <= endY; y++) {
                float x = _getXByY(y);
                wRect.set(x, y, x + end.width(), y + end.height());
                if(rectStatic.intersect(wRect) || wRect.intersect(rectStatic))
                    return true;
            }
        }

        return false;
    }


    public boolean isTouch(RectF touchSurface)
    {
        if(Math.abs(GetXTransition()) > Math.abs(GetYTransition())) {

            int startX = (int) Math.floor(Math.min(start.left, end.left));
            int endX = (int) Math.ceil(Math.max(start.left, end.left));

            for (int x = startX; x <= endX ; x++) {
                float y = _getYByX(x);

                if(isTouchDetected(touchSurface, x, y, x+end.width(), y+end.height()))
                    return true;
            }
        }
        else {
            int startY = (int) Math.floor(Math.min(start.top, end.top));
            int endY = (int) Math.ceil(Math.max(start.top, end.top));

            for (int y = startY; y <= endY; y++) {
                float x = _getXByY(y);
                if(isTouchDetected(touchSurface, x, y, x+end.width(), y+end.height()))
                    return true;
            }
        }

        return false;
    }


    private boolean isTouchDetected(RectF surface, float left, float top, float right, float bottom)
    {
        boolean isInOx =
                (left>surface.left && left<surface.right) || (right>surface.left && right<surface.right);
        boolean isInOy =
                bottom>=surface.top && bottom<=surface.bottom;

        return isInOx && isInOy;
    }


    private float _getYByX(float X)
    {
        /* equation of line */
        /*
        *  Y-y0     X-x0
        * ------ = ------
        * y1-y0    x1-x0
        *
        * */
        if(end.left-start.left==0)
            return end.top;

        return ((X-start.left)*(end.top-start.top))/(end.left-start.left) + start.top;
    }
    private float _getXByY(float Y)
    {
        /* equation of line */
        /*
        *  Y-y0     X-x0
        * ------ = ------
        * y1-y0    x1-x0
        *
        * */
        if(end.top-start.top==0)
            return end.left;

        return ((Y-start.top)*(end.left-start.left))/(end.top-start.top) + start.left;
    }

}
