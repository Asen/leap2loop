package com.d9soft.leap2loop.util;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;

import com.d9soft.leap2loop.App;
import com.d9soft.leap2loop.R;

public class Audio {

    public enum Cover {UNDEFINED, MENU, GAME}
    /////////////////////////////////////////////////////
    private static final int SOUND_LEAP = R.raw.leap;
    private static final int SOUND_SCORE = R.raw.point;
    private static final int SOUND_CRASH = R.raw.crash;
    private static final int SOUND_ICE_CRACK = R.raw.icecrack;
    private static final int SOUND_BTN_CLICK = R.raw.click;
    private static final int SOUND_COVER_GAME = R.raw.cover_game;
    private static final int SOUND_COVER_MENU = R.raw.cover_menu;
    /////////////////////////////////////////////////////
    private static int leapSndId, scoreSndId, crashSndId, iceCrackId, clickSndId;
    private static SoundPool soundPool = null;
    private static MediaPlayer coverSound = null;
    private static Cover cCover = Cover.UNDEFINED;
    private static boolean
            isSoundsOn = L2LData.getSoundState(),
            isMusicOn = L2LData.getMusicState();

    public static void InitSounds()
    {
        leapSndId = scoreSndId = crashSndId = iceCrackId = -1;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes attrs = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            soundPool = new SoundPool.Builder().
                    setAudioAttributes(attrs).
                    setMaxStreams(5).
                    build();
        }
        else
            soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);

        final Context context = App.getInstance().getApplicationContext();
        iceCrackId = soundPool.load(context, SOUND_ICE_CRACK, 1);
        leapSndId = soundPool.load(context, SOUND_LEAP, 1);
        scoreSndId = soundPool.load(context, SOUND_SCORE, 1);
        crashSndId = soundPool.load(context, SOUND_CRASH, 1);
        clickSndId = soundPool.load(context, SOUND_BTN_CLICK, 1);
    }

    public static void SetSoundEnabled(boolean isEnabled)
    {
        isSoundsOn = isEnabled;
    }

    public static void SetMusicEnabled(boolean isEnabled)
    {
        isMusicOn = isEnabled;
        if(isMusicOn)
            ResumeCoverSound();
        else
            PauseCoverSound();
    }

    public static void PauseAll()
    {
        PauseCoverSound();
    }

    public static void ResumeAll()
    {
        ResumeCoverSound();
    }

    public static void PlayLeap()
    {
        if(leapSndId==-1 || !isSoundsOn)
            return;

        float leftVolume = 0.4f;
        float rightVolume = 0.4f;
        int priority = 1;
        int noLoop = 0;
        float playSpeed = 1.5f;
        soundPool.play(leapSndId, leftVolume, rightVolume, priority, noLoop, playSpeed);
    }

    public static void PlayScore()
    {
        if(scoreSndId==-1 || !isSoundsOn)
            return;

        float leftVolume = 0.25f;
        float rightVolume = 0.25f;
        int priority = 1;
        int noLoop = 0;
        float playSpeed = 0.5f;
        soundPool.play(scoreSndId, leftVolume, rightVolume, priority, noLoop, playSpeed);
    }

    public static void PlayCrash()
    {
        if(crashSndId==-1 || !isSoundsOn)
            return;

        float leftVolume = 0.25f;
        float rightVolume = 0.25f;
        int priority = 1;
        int noLoop = 0;
        float playSpeed = 1f;
        soundPool.play(crashSndId, leftVolume, rightVolume, priority, noLoop, playSpeed);
    }

    public static void PlayIceCrack()
    {
        if(!isSoundsOn)
            return;

        new Thread(new Runnable() {
            @Override
            public void run() {

                while(iceCrackId==-1){ continue; }

                float leftVolume = 0.25f;
                float rightVolume = 0.25f;
                int priority = 1;
                int noLoop = 0;
                float playSpeed = 1.5f;
                soundPool.play(iceCrackId, leftVolume, rightVolume, priority, noLoop, playSpeed);

            }
        }).run();

    }

    public static void PlayClick()
    {
        if(clickSndId == -1 || !isSoundsOn)
            return;

        float leftVolume = 0.85f;
        float rightVolume = 0.85f;
        int priority = 1;
        int noLoop = 0;
        float playSpeed = 0.99f;
        soundPool.play(clickSndId, leftVolume, rightVolume, priority, noLoop, playSpeed);
    }

    public static void ReStartCover(Cover cover)
    {
        PauseCoverSound();

        if(cover!=cCover)
        {
            Context context = App.getInstance().getBaseContext();

            switch (cover)
            {
                case MENU:
                    coverSound = MediaPlayer.create(context, SOUND_COVER_MENU);
                    break;
                case GAME:
                    coverSound = MediaPlayer.create(context, SOUND_COVER_GAME);
                    coverSound.setVolume(0.08f, 0.08f);
                    break;
                default:
                    App.getInstance().Assert(false, "Undefined cover name");
            }
        }

       if (isCoverPlaying() && coverSound.getCurrentPosition() > 0)
           coverSound.seekTo(0);

       ResumeCoverSound();
    }

    public static void ResumeCoverSound()
    {
        if(coverSound!=null && !isCoverPlaying() && isMusicOn) {
            coverSound.start();
            coverSound.setLooping(true);
        }
    }

    public static void PauseCoverSound()
    {
        if(coverSound!=null && isCoverPlaying())
            coverSound.pause();
    }

    /* Safe check independent of current state */
    private static boolean isCoverPlaying()
    {
        try{
            return coverSound.isPlaying();
        }catch (Exception e)
        {
            App.getInstance().Assert(false, "Audio error: "+e.getMessage());
            return false;
        }
    }

}
