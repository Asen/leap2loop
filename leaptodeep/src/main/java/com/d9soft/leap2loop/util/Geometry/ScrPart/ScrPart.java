package com.d9soft.leap2loop.util.Geometry.ScrPart;


public abstract class ScrPart {

    protected float partOfScreen;

    public ScrPart(float p_partOfScreen)
    {
        partOfScreen = p_partOfScreen;
    }

    public ScrPart(ScrPart copy)
    {
        if(copy!=null)
            this.partOfScreen = copy.partOfScreen;
    }

    public abstract float Calc();

}
