package com.d9soft.leap2loop.util.Geometry.CCount;

/*
*
* This class represents the geometric
* size of L2L Character`s width.
*
* For more convenient calculations.
*
* */

import com.d9soft.leap2loop.game.L2LConfig;

public class CCountW extends CCount {

    public CCountW(float p_charactersCount) {
        super(p_charactersCount);
    }

    public CCountW(CCount copy) {
        super(copy);
    }

    public static float Calc(float p_charsCount){
        return p_charsCount * L2LConfig.GetCharacterWidth();
    }

    @Override
    public float Calc() {
        return charactersCount * L2LConfig.GetCharacterWidth();
    }
}
