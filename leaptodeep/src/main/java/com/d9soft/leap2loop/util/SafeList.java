package com.d9soft.leap2loop.util;

import java.util.ArrayList;

public class SafeList<E> extends ArrayList<E> {

    public E getLast() {

        if(this.size()>0)
            return this.get(this.size()-1);

        return null;

    }

    public E getFirst() {

        if(this.size()>0)
            return this.get(0);

        return null;

    }
}
