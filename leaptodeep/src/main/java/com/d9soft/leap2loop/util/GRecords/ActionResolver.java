package com.d9soft.leap2loop.util.GRecords;

public interface ActionResolver {

    /** Status of user`s signing */
    public boolean getSignedInGPGS();

    /** Sign in */
    public void loginGPGS();

    /** Post score to score table */
    public void submitScoreGPGS(int score);

    /**
     * Achievement unlock
     *
     * @param achievementId
     *            Achievement`s ID is there: games-ids.xml
     */
    public void unlockAchievementGPGS(String achievementId);

    /** Show activity with score board */
    public void getLeaderboardGPGS();

    /** Show activity with achievements */
    public void getAchievementsGPGS();
}